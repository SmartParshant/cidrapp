package com.cidr;
import android.os.Bundle;
import org.devio.rn.splashscreen.SplashScreen;
import com.reactnativenavigation.controllers.SplashActivity;

public class MainActivity extends SplashActivity {
 @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this);  // here
        super.onCreate(savedInstanceState);
    }
}
