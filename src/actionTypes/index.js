/**********************Common*************************/

export const INITIALIZED = "example.app.INITIALIZED";
export const ROOT_CHANGED = "example.app.ROOT_CHANGED";

export const USER_REGISTERATION = "USER_REGISTERATION";
export const SET_NAVIGATOR = "SET_NAVIGATOR";
export const SET_SOCKET = "SET_SOCKET";

//token registration
export const UPDATE_NOTIFICATIONS_INFO = "UPDATE_NOTIFICATIONS_INFO";

export const SAVE_USER = "SAVE_USER";
export const SAVE_ACCESS_TOKEN = "SAVE_ACCESS_TOKEN";

export const SIGNUP_REQUEST = "SIGNUP_REQUEST";
export const SIGNUP_SUCESS = "SIGNUP_SUCESS";
export const SIGNUP_FAIL = "SIGNUP_FAIL";

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCESS = "LOGIN_SUCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";

export const ACCESS_CODE_REQUEST = "ACCESS_CODE_REQUEST";
export const ACCESS_CODE_SUCESS = "ACCESS_CODE_SUCESS";
export const ACCESS_CODE_FAIL = "ACCESS_CODE_FAIL";

export const FORGOT_REQUEST = "FORGOT_REQUEST";
export const FORGOT_SUCESS = "FORGOT_SUCESS";
export const FORGOT_FAIL = "FORGOT_FAIL";

export const OTP_REQUEST = "OTP_REQUEST";
export const OTP_SUCESS = "OTP_SUCESS";
export const OTP_FAIL = "OTP_FAIL";

export const CHANGE_PASSWORD_REQUEST = "CHANGE_PASSWORD_REQUEST";
export const CHANGE_PASSWORD_SUCESS = "CHANGE_PASSWORD_SUCESS";
export const CHANGE_PASSWORD_FAIL = "CHANGE_PASSWORD_FAIL";

export const PROFILE_IMAGE_REQUEST = "PROFILE_IMAGE_REQUEST";
export const PROFILE_IMAGE_SUCESS = "PROFILE_IMAGE_SUCESS";
export const PROFILE_IMAGE_FAIL = "PROFILE_IMAGE_FAIL";
export const PROFILE_IMAGE_URL = "PROFILE_IMAGE_URL";

export const NAME_UPDATE_REQUEST = "NAME_UPDATE_REQUEST";
export const NAME_UPDATE_REQUEST_SUCESS = "NAME_UPDATE_REQUEST_SUCESS";
export const NAME_UPDATE_REQUEST_FAIL = "NAME_UPDATE_REQUEST_FAIL";
export const UPDATE_NAME = "UPDATE_NAME";

export const MOBILE_UPDATE_REQUEST = "MOBILE_UPDATE_REQUEST";
export const MOBILE_UPDATE_REQUEST_SUCESS = "MOBILE_UPDATE_REQUEST_SUCESS";
export const MOBILE_UPDATE_REQUEST_FAIL = "MOBILE_UPDATE_REQUEST_FAIL";
export const UPDATE_MOBILE = "UPDATE_MOBILE";

export const SET_RATE_SCREEN = "SET_RATE_SCREEN";
/**********************user*************************/

export const RIDER_SOURCE = "RIDER_SOURCE";
export const RIDER_DESTINATION = "RIDER_DESTINATION";
export const RIDER_LOCATION_TYPE = "RIDER_LOCATION_TYPE";
export const RIDER_COUNT = "RIDER_COUNT";
export const UPDATE_RIDE_TIME = "UPDATE_RIDE_TIME";

export const PICKUP_POINT_REQUEST = "PICKUP_POINT_REQUEST";
export const PICKUP_POINT = "PICKUP_POINT";
export const PICKUP_POINT_SUCESS = "PICKUP_POINT_SUCESS";
export const PICKUP_POINT_REQUEST_FAILS = "PICKUP_POINT_REQUEST_FAILS";

export const PROVIDER_REQUEST = "PROVIDER_REQUEST";
export const PROVIDERS = "PROVIDERS";
export const PROVIDER_SUCESS = "PROVIDER_SUCESS";
export const PROVIDER_REQUEST_FAILS = "PROVIDER_REQUEST_FAILS";
export const USER_PROVIDER = "USER_PROVIDER";

export const RIDER_RIDE_HISTORY_REQUEST = "RIDER_RIDE_HISTORY_REQUEST";
export const RIDER_RIDE_HISTORY_SUCESS = "RIDER_RIDE_HISTORY_SUCESS";
export const RIDER_RIDE_HISTORY_FAIL = "RIDER_RIDE_HISTORY_FAIL";
export const RIDER_HISTORY = "RIDER_HISTORY";

export const RIDER_RATE_REVIEW_REQUEST = "RIDER_RATE_REVIEW_REQUEST";
export const RIDER_RATE_REVIEW_SUCESS = "RIDER_RATE_REVIEW_SUCESS";
export const RIDER_RATE_REVIEW_FAIL = "RIDER_RATE_REVIEW_FAIL";
export const RIDER_RATE_REVIEW = "RIDER_RATE_REVIEW";
/**********************Driver***********************/

export const SHUTTLE_LIST_REQUEST = "SHUTTLE_LIST_REQUEST";
export const SHUTTLE_LIST_SUCESS = "SHUTTLE_LIST_SUCESS";
export const SHUTTLE_LIST_FAIL = "SHUTTLE_LIST_FAIL";
export const SHUTTLE_LIST = "SHUTTLE_LIST";

export const SELECTED_SHUTTLE = "SELECTED_SHUTTLE";
export const TRIP_UPDATE_REQUEST = "TRIP_UPDATE_REQUEST";
export const TRIP_UPDATE_REQUEST_SUCCESS = "TRIP_UPDATE_REQUEST_SUCCESS";
export const TRIP_UPDATE_REQUEST_FAIL = "TRIP_UPDATE_REQUEST_FAIL";
export const UPDATE_TRIP_DATA = "UPDATE_TRIP_DATA";

export const RIDE_REQUEST_LIST = "RIDE_REQUEST_LIST";
export const RIDE_REQUEST_LIST_REQUEST = "RIDE_REQUEST_LIST_REQUEST";
export const RIDE_REQUEST_LIST_SUCESS = "RIDE_REQUEST_LIST_SUCESS";
export const RIDE_REQUEST_LIST_FAIL = "RIDE_REQUEST_LIST_FAIL";

export const TERMINAL_RIDE_REQUEST_LIST = "TERMINAL_RIDE_REQUEST_LIST";
export const TERMINAL_RIDE_REQUEST_LIST_REQUEST = "TERMINAL_RIDE_REQUEST_LIST_REQUEST";
export const TERMINAL_RIDE_REQUEST_LIST_SUCESS = "TERMINAL_RIDE_REQUEST_LIST_SUCESS";
export const TERMINAL_RIDE_REQUEST_LIST_FAIL = "TERMINAL_RIDE_REQUEST_LIST_FAIL";

export const TRIP_HISTORY_REQUEST = "TRIP_HISTORY_REQUEST";
export const TRIP_HISTORY_REQUEST_SUCESS = "TRIP_HISTORY_REQUEST_SUCESS";
export const TRIP_HISTORY_REQUEST_FAIL = "TRIP_HISTORY_REQUEST_FAIL";
export const TRIP_HISTORY = "TRIP_HISTORY";

/*CMS Loader*/
export const SHOW_CMS_LOADER = "SHOW_CMS_LOADER";
export const HIDE_CMS_LOADER = "HIDE_CMS_LOADER";

/**********************Admin*************************/
export const ADMIN_SHUTTLE_LISTING_RESET = "ADMIN_SHUTTLE_LISTING_RESET";
export const ADMIN_SHUTTLE_LISTING_REQUEST = "ADMIN_SHUTTLE_LISTING_REQUEST";
export const ADMIN_SHUTTLE_LISTING_SUCESS = "ADMIN_SHUTTLE_LISTING_SUCESS";
export const ADMIN_SHUTTLE_LIST = "ADMIN_SHUTTLE_LIST";
export const ADMIN_SHUTTLE_LISTING_FAIL = "ADMIN_SHUTTLE_LISTING_FAIL";
export const ADMIN_SHUTTLE_META = "ADMIN_SHUTTLE_META";

export const ADMIN_DRIVER_LISTING_RESET = "ADMIN_DRIVER_LISTING_RESET";
export const ADMIN_DRIVER_LISTING_REQUEST = "ADMIN_DRIVER_LISTING_REQUEST";
export const ADMIN_DRIVER_LISTING_SUCESS = "ADMIN_DRIVER_LISTING_SUCESS";
export const ADMIN_DRIVER_LIST = "ADMIN_DRIVER_LIST";
export const ADMIN_DRIVER_LISTING_FAIL = "ADMIN_DRIVER_LISTING_FAIL";
export const ADMIN_DRIVER_META = "ADMIN_DRIVER_META";

export const ADMIN_RIDER_LISTING_RESET = "ADMIN_RIDER_LISTING_RESET";
export const ADMIN_RIDER_LISTING_REQUEST = "ADMIN_RIDER_LISTING_REQUEST";
export const ADMIN_RIDER_LISTING_SUCESS = "ADMIN_RIDER_LISTING_SUCESS";
export const ADMIN_RIDER_LIST = "ADMIN_RIDER_LIST";
export const ADMIN_RIDER_LISTING_FAIL = "ADMIN_RIDER_LISTING_FAIL";
export const ADMIN_RIDER_META = "ADMIN_RIDER_META";
export const UPDATE_FILTERS = "UPDATE_FILTERS";

export const ADMIN_ACTIVE_TRIPS_REQUEST = "ADMIN_ACTIVE_TRIPS_REQUEST";
export const ADMIN_ACTIVE_TRIPS = "ADMIN_ACTIVE_TRIPS";
export const ADMIN_ACTIVE_TRIPS_SUCCESS = "ADMIN_ACTIVE_TRIPS_SUCCESS";
export const ADMIN_ACTIVE_TRIPS_FAIL = "ADMIN_ACTIVE_TRIPS_FAIL";

export const ADMIN_UPDATE_CURRENT_TRIP = "ADMIN_UPDATE_CURRENT_TRIP";

export const ADMIN_CURRENT_TRIP_ROUTE = "ADMIN_CURRENT_TRIP_ROUTE";
export const ADMIN_CURRENT_TRIP_ROUTE_REQUEST = "ADMIN_CURRENT_TRIP_ROUTE_REQUEST";
export const ADMIN_CURRENT_TRIP_ROUTE_SUCESS = "ADMIN_CURRENT_TRIP_ROUTE_SUCESS";
export const ADMIN_CURRENT_TRIP_ROUTE_FAIL = "ADMIN_CURRENT_TRIP_ROUTE_FAIL";
/*****************RESET_REDUCERS ******************/
/**********************user*************************/
export const RESET_APP = "RESET_APP";
export const RESET_USER = "RESET_USER";

export const RESET_RIDER_DATA = "RESET_RIDER_DATA";
export const RESET_RIDER_TRIP = "RESET_RIDER_TRIP";
export const RESET_TERMINAL_LIST = "RESET_TERMINAL_LIST";
export const RESET_TERMINAL_LISTING = "RESET_TERMINAL_LISTING";
export const RESET_SHUTTLE = "RESET_SHUTTLE";
export const RESET_TRIP = "RESET_TRIP";
export const SAVE_RIDER_DRIVER = "SAVE_RIDER_DRIVER";
export const SAVE_RIDER_SHUTTLE = "SAVE_RIDER_SHUTTLE";

/**********************driver*************************/

export const UPDATE_RIDES_META = "UPDATE_RIDES_META";
export const UPDATE_RIDES = "UPDATE_RIDES";
export const REMOVE_RIDES = "REMOVE_RIDES";
export const ADD_RIDE = "ADD_RIDE";
/**********************admin*************************/

export const RESET_LISTING = "RESET_LISTING";

/*****************Socket Events*******************/
/**********************user*************************/
export const UPDATE_GPS_LOCATION = "UPDATE_GPS_LOCATION";
export const UPDATE_TRIP = "UPDATE_TRIP";
export const UPDATE_SHUTTLE_LOCATION = "UPDATE_SHUTTLE_LOCATION";
export const UPDATE_REGION = "UPDATE_REGION";
export const UPDATE_RECENT_UPDATE = "UPDATE_RECENT_UPDATE";
/**********************Driver*************************/
export const UPDATE_CURRENT_TERMINAL = "UPDATE_CURRENT_TERMINAL";
/**********************Admin*************************/
