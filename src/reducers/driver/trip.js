/*
AuthorName : Suraj Sanwal
FileName: reducer.js
Description: Contains the reducer regarding the trip
Date : 11 Sept 2018  
*/

import * as Types from "../../actionTypes";
const initialState = {
  myShuttle: {},
  rides: [],
  meta: {},
  driverRoute: [],
  region: {
    latitude: 36.1233,
    longitude: 71.2343,
    latitudeDelta: 0.12,
    longitudeDelta: 0.12
  },
  currentTerminal: null
};

var trips = (state = initialState, action) => {
  switch (action.type) {
    case Types.UPDATE_TRIP_DATA:
      return {
        ...state,
        ...action.payload
      };
    case Types.SELECTED_SHUTTLE:
      return {
        ...state,
        myShuttle: action.payload
      };
    case Types.RIDE_REQUEST_LIST:
      return {
        ...state,
        ...action.payload
      };
    case Types.UPDATE_RIDES:
      return {
        ...state,
        rides: [action.payload, ...state.rides]
      };
    case Types.REMOVE_RIDES:
      // Find item index using _.findIndex
      return {
        ...state,
        rides: action.payload
      };
    case Types.UPDATE_RIDES_META:
      return {
        ...state,
        meta: {
          ...state.meta,
          newRequestsCount: action.payload
        }
      };
    case Types.RESET_TRIP:
      return {
        ...initialState
      };
    case Types.UPDATE_REGION:
      return {
        ...state,
        region: action.payload
      };
    case Types.UPDATE_CURRENT_TERMINAL:
      return {
        ...state,
        currentTerminal: action.payload
      };
    default:
      return state;
  }
};
export default trips;
