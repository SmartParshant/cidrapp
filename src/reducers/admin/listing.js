import * as types from "../../actionTypes";

const initialState = {
  shuttles: [],
  drivers: [],
  riders: [],
  shuttleMeta: {},
  driverMeta: {},
  ridersMeta: {},
  filters: {},
  activeTrips: [],
  region: {},
  currentTrip: "",
  tripData: {},
  tripRoute: {}
};

export default function listing(state = initialState, action = {}) {
  switch (action.type) {
    case types.ADMIN_SHUTTLE_LIST:
      return {
        ...state,
        shuttles: state.shuttles.concat(action.payload)
      };
    case types.ADMIN_SHUTTLE_META:
      return {
        ...state,
        shuttleMeta: action.payload
      };
    case types.ADMIN_RIDER_META:
      return {
        ...state,
        ridersMeta: action.payload
      };
    case types.ADMIN_DRIVER_META:
      return {
        ...state,
        driverMeta: action.payload
      };
    case types.ADMIN_DRIVER_LIST:
      return {
        ...state,
        drivers: action.payload
      };
    case types.ADMIN_RIDER_LIST:
      return {
        ...state,
        riders: action.payload
      };
    case types.ADMIN_SHUTTLE_LISTING_RESET:
      return {
        ...state,
        shuttles: []
      };
    case types.UPDATE_FILTERS:
      return { ...state, filters: action.payload };
    case types.ADMIN_ACTIVE_TRIPS:
      return { ...state, activeTrips: action.payload };
    case types.UPDATE_REGION:
      return { ...state, region: action.payload };
    case types.ADMIN_UPDATE_CURRENT_TRIP:
      return { ...state, currentTrip: action.payload };
    case types.RIDE_REQUEST_LIST:
      return { ...state, tripData: action.payload };
    case types.ADMIN_CURRENT_TRIP_ROUTE:
      return { ...state, tripRoute: action.payload };
    case types.RESET_LISTING:
      return {
        ...initialState
      };
    default:
      return state;
  }
}
