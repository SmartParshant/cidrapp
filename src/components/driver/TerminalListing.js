/*
Name : Suraj Sanwal
File Name : DashBoard.js
Description : Contains the Terminal Listing screen
Date : 20 Oct 2018
*/
import React from "react";
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity } from "react-native";
import moment from "moment";

import Constants from "../../constants";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import AuthButton from "../../components/common/AuthButton";
import DriverSocket from "../../helpers/socket/driver";

const renderItem = (item, chat, userType) => {
  return (
    <View style={Styles.flatlistView} key={item._id}>
      <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
        {item.tripRequestStatus === Constants.AppCosntants.RideStatus.Request ? (
          <View style={[Styles.clockImgView]}>
            <Image source={Constants.Images.RideInfo.Clock} />
          </View>
        ) : item.tripRequestStatus === Constants.AppCosntants.RideStatus.Accepted ? (
          <View style={[Styles.clockImgView, { backgroundColor: Constants.Colors.green }]}>
            <Image source={Constants.Images.Common.Accept} />
          </View>
        ) : item.tripRequestStatus === Constants.AppCosntants.RideStatus.Cancelled ||
        item.tripRequestStatus === Constants.AppCosntants.RideStatus.Rejected ? (
          <View style={[Styles.clockImgView, { backgroundColor: Constants.Colors.red }]}>
            <Image source={Constants.Images.Common.Cancel} />
          </View>
        ) : null}

        <View style={{ flex: 0.8, justifyContent: "center" }}>
          <Text numberOfLines={1} style={Styles.nameTxt}>
            {item.riderDetails && item.riderDetails.name}
          </Text>
        </View>

        <View
          style={{
            flex: 0.15,
            alignItems: "center",
            justifyContent: "space-around",
            flexDirection: "row"
          }}
        >
          <Text numberOfLines={1} style={Styles.boldTxt}>
            {item.seatBooked}
          </Text>
          <View
            style={{
              height: Constants.BaseStyle.DEVICE_HEIGHT * 0.04,
              width: Constants.BaseStyle.DEVICE_HEIGHT * 0.04,
              borderRadius: moderateScale(100),
              backgroundColor: Constants.Colors.gray,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image source={Constants.Images.RideInfo.Man} resizeMode={"contain"} />
          </View>
        </View>
      </View>
      <View style={{ marginLeft: moderateScale(35) }}>
        <View
          style={{
            flexDirection: "row",
            paddingVertical: moderateScale(10),
            alignItems: "center"
          }}
        >
          <Image
            source={Constants.Images.Common.Source}
            style={{
              height: Constants.BaseStyle.DEVICE_HEIGHT * 0.02,
              width: Constants.BaseStyle.DEVICE_HEIGHT * 0.02
            }}
          />
          <View
            style={{
              flex: 0.5,
              paddingHorizontal: moderateScale(10)
            }}
          >
            <Text numberOfLines={1} style={Styles.regularTxt}>
              {item.srcLoc && item.srcLoc.name && item.srcLoc.name.trim()}
            </Text>
          </View>
          <Image source={Constants.Images.TerminalDetail.Pin} />
          <View
            style={{
              flex: 0.5,
              paddingHorizontal: moderateScale(10)
            }}
          >
            <Text numberOfLines={1} style={Styles.regularTxt}>
              {item.destLoc && item.destLoc.name && item.destLoc.name.trim()}
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between"
          }}
        >
          <View style={{ flexDirection: "row", justifyContent: "space-between", flex: 0.33, alignItems: "center" }}>
            <Image
              source={Constants.Images.RideInfo.ClockGray}
              style={{
                height: Constants.BaseStyle.DEVICE_HEIGHT * 0.03,
                width: Constants.BaseStyle.DEVICE_HEIGHT * 0.03
              }}
            />
            <Text numberOfLines={1} style={Styles.regularTxt}>
              {moment(item.requestTime).format("hh:mm A")}
            </Text>
          </View>
          {item.tripRequestStatus === Constants.AppCosntants.RideStatus.Request ? (
            <View style={{ flexDirection: "row", justifyContent: "space-around", flex: 0.35, alignItems: "center" }}>
              <Text numberOfLines={1} style={Styles.boldTxt}>
                {moment.duration(moment().diff(moment(item.requestTime)))._data.minutes} Min
              </Text>
            </View>
          ) : null}
          {userType === Constants.AppCosntants.UserTypes.Driver &&
          item.tripRequestStatus === Constants.AppCosntants.RideStatus.Request ? (
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
                flex: 0.25,
                alignItems: "center"
                // paddingRight: moderateScale(15)
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  DriverSocket.driverRejectTripRequest(item._id);
                }}
                style={{
                  backgroundColor: "#FF6965",
                  height: Constants.BaseStyle.DEVICE_HEIGHT * 0.04,
                  width: Constants.BaseStyle.DEVICE_HEIGHT * 0.04,
                  borderRadius: moderateScale(100),
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Image source={Constants.Images.Common.Cancel} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  //alert("under development");
                  DriverSocket.driverAcceptTripRequest(item._id);
                }}
                style={{
                  height: Constants.BaseStyle.DEVICE_HEIGHT * 0.04,
                  width: Constants.BaseStyle.DEVICE_HEIGHT * 0.04,
                  borderRadius: moderateScale(100),
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: Constants.Colors.Yellow
                }}
              >
                <Image source={Constants.Images.Common.Accept} />
              </TouchableOpacity>
            </View>
          ) : userType === Constants.AppCosntants.UserTypes.Admin &&
          item.tripRequestStatus !== Constants.AppCosntants.RideStatus.Rejected &&
          item.tripRequestStatus !== Constants.AppCosntants.RideStatus.Cancelled ? (
            <TouchableOpacity
              onPress={() => alert("underdevelopment")}
              style={{
                height: Constants.BaseStyle.DEVICE_HEIGHT * 0.05,
                width: Constants.BaseStyle.DEVICE_HEIGHT * 0.05,
                borderRadius: moderateScale(100),
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: Constants.Colors.Yellow
              }}
            >
              <Image source={Constants.Images.Common.WhiteChat} />
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    </View>
  );
  // }
};

export const TerminalListing = props => {
  let { meta, rides, terminal, message, hideMeta, chat, userType } = props;
  return (
    <View style={{ backgroundColor: Constants.Colors.White, flex: 1 }}>
      {!hideMeta ? (
        meta.totalSeats ? (
          <View style={Styles.headerView}>
            <View
              style={[
                Styles.noOfPassengerView,
                { flex: 1, alignItems: "flex-start", paddingHorizontal: moderateScale(20) }
              ]}
            >
              <Text numberOfLines={1} style={Styles.noOfPassengerTxt}>
                {`${meta.totalSeats} ${message}`}
              </Text>
            </View>
          </View>
        ) : (
          <View style={Styles.headerView}>
            <View style={Styles.noOfPassengerView}>
              <Text numberOfLines={1} style={Styles.noOfPassengerTxt}>
                {meta && meta.onBoardCount} Passengers
              </Text>
            </View>
            <View
              style={{
                flex: 0.2,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text numberOfLines={1} style={Styles.boldTxt}>
                {meta && meta.newRequestsCount} New
              </Text>
            </View>

            <View style={Styles.acceptBtnView}>
              {userType === Constants.AppCosntants.UserTypes.Driver && meta && meta.newRequestsCount > 0 ? (
                <AuthButton
                  buttonStyle={Styles.buttonStyle}
                  gradientStyle={Styles.gradientStyle}
                  gradientColors={[Constants.Colors.Primary, Constants.Colors.Primary]}
                  buttonName={"Accept All"}
                  onPress={() => {
                    //accept all requests
                    DriverSocket.acceptAllTripRequests((terminal && terminal._id) || "");
                  }}
                  textStyle={{ color: Constants.Colors.White }}
                />
              ) : userType === Constants.AppCosntants.UserTypes.Admin ? (
                <TouchableOpacity
                  onPress={() => alert("underdevelopment")}
                  style={{
                    height: Constants.BaseStyle.DEVICE_HEIGHT * 0.05,
                    width: Constants.BaseStyle.DEVICE_HEIGHT * 0.05,
                    borderRadius: moderateScale(100),
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: Constants.Colors.Primary,
                    bottom: moderateScale(12),
                    left: moderateScale(35)
                  }}
                >
                  <Image source={Constants.Images.Common.WhiteChat} />
                </TouchableOpacity>
              ) : null}
            </View>
          </View>
        )
      ) : null}

      {rides.length ? (
        <FlatList
          data={rides}
          renderItem={({ item }) => {
            return renderItem(item, chat, userType);
          }}
          style={{ marginBottom: moderateScale(2) }}
          numColumns={1}
          keyExtractor={item => item._id}
        />
      ) : (
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Text style={Styles.nameTxt}>No Request Found</Text>
        </View>
      )}
    </View>
  );
};

const Styles = StyleSheet.create({
  buttonStyle: {
    flex: 1,
    paddingHorizontal: moderateScale(10)
  },
  gradientStyle: {
    borderRadius: moderateScale(5),
    padding: moderateScale(10)
  },
  flatlistView: {
    flexDirection: "column",
    borderBottomWidth: 0.4,
    borderBottomColor: Constants.Colors.placehoder,
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(25),
    backgroundColor: Constants.Colors.White,
    // alignItems: "center"
    justifyContent: "space-between"
  },
  clockImgView: {
    //flex: 0.2,
    paddingVertical: moderateScale(10),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Constants.Colors.gray,
    height: Constants.BaseStyle.DEVICE_HEIGHT * 0.04,
    width: Constants.BaseStyle.DEVICE_HEIGHT * 0.04,
    borderRadius: moderateScale(100),
    marginTop: moderateScale(15)
    // marginRight: moderateScale(2)
  },
  nameTxt: {
    ...Constants.Fonts.TitilliumWebSemiBold,
    color: Constants.Colors.Primary,
    fontSize: moderateScale(19)
  },
  boldTxt: {
    ...Constants.Fonts.TitilliumWebSemiBold,
    fontSize: moderateScale(17),
    color: Constants.Colors.Primary
  },
  regularTxt: {
    ...Constants.Fonts.TitilliumWebRegular,
    color: Constants.Colors.placehoder,
    fontSize: moderateScale(17)
  },
  acceptBtnView: {
    flex: 0.4,
    justifyContent: "center",
    alignItems: "center",
    top: moderateScale(15)
  },
  noOfPassengerTxt: {
    ...Constants.Fonts.TitilliumWebRegular,
    color: Constants.Colors.placehoder,
    fontSize: moderateScale(17)
  },
  headerView: {
    height: Constants.BaseStyle.DEVICE_HEIGHT * 0.1,
    flexDirection: "row",
    borderBottomWidth: 0.4,
    borderBottomColor: Constants.Colors.White
  },
  noOfPassengerView: {
    flex: 0.4,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default TerminalListing;
