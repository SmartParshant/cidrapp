/*
Name : Suraj Sanwal 
File Name : RiderRideAccepted.js
Description : Contains the Ride Accepted view.
Date : 17 oct 2018
*/
import React, { Component } from "react";
import { View, Image, Text, StyleSheet } from "react-native";
import moment from "moment";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";
import TimerMixin from "react-timer-mixin";
import reactMixin from "react-mixin";

import Constants from "../../constants";
import AuthButton from "../common/AuthButton";
import * as appActions from "../../actions";
import { moderateScale } from "../../helpers/ResponsiveFonts";

class RiderRideAccepted extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDateTimePickerVisible: false,
      waitTime: {
        min: 0,
        sec: 0
      }
    };
  }

  static navigatorStyle = {
    navBarHidden: true,
    screenBackgroundColor: "transparent",
    modalPresentationStyle: "overFullScreen"
  };

  componentDidMount() {}

  cancelRide = _.debounce(() => {
    this.setTimeout(() => {
      this.props.navigator.showModal({
        screen: "CancelRide",
        animationType: "slide-up",
        navigatorStyle: {
          statusBarColor: "transparent",
          navBarHidden: true,
          screenBackgroundColor: "transparent",
          modalPresentationStyle: "overFullScreen"
        }
      });
    }, 500);
    this.props.navigator.dismissModal();
  });

  moveToChatWindow = _.debounce(() => {
    this.setTimeout(() => {
      this.props.navigator.handleDeepLink({
        link: "ChatWindow",
        payload: {
          passProps: {},
          push: true
        }
      });
    }, 500);
    this.props.navigator.dismissModal();
  });

  render() {
    let { riderTrip } = this.props;
    let { requestTime, seatBooked, driver, shuttle } = riderTrip;
    let { name, profileUrl } = driver;
    // let { waitTime } = this.state;
    return (
      <View style={Styles.container}>
        <View style={Styles.modalView}>
          <View style={Styles.sideMenuImageContainer}>
            <View style={Styles.profileImg}>
              <Image source={{ uri: profileUrl }} style={Styles.imgAvatar} resizeMode={"center"} />
            </View>
          </View>
          <View
            style={{
              backgroundColor: Constants.Colors.White,
              width: Constants.BaseStyle.DEVICE_WIDTH,
              justifyContent: "space-between",
              //alignItems: "center",
              flexDirection: "column",
              borderRadius: moderateScale(10)
            }}
          >
            <View
              style={{
                flex: 0.5,
                flexDirection: "row",
                alignItems: "flex-end",
                justifyContent: "space-between"
              }}
            >
              <View style={Styles.timePersonContainer}>
                <Text style={Styles.userName} numberOfLines={1}>
                  {name}
                </Text>
              </View>
              <View style={[Styles.timePersonContainer, { flex: 0.77, justifyContent: "flex-end" }]}>
                <Text style={Styles.userName}>{shuttle.name + "" + shuttle.vehicleNo}</Text>
                <View
                  style={[
                    Styles.timeManContainer,
                    {
                      height: moderateScale(40),
                      width: moderateScale(40),
                      backgroundColor: Constants.Colors.transparent,
                      borderColor: Constants.Colors.gray,
                      borderWidth: 0.4,
                      justifyContent: "center",
                      alignItems: "center",
                      paddingVertical: moderateScale(5)
                    }
                  ]}
                >
                  <Image
                    source={Constants.Images.RideInfo.InActiveShuttle}
                    style={{ height: moderateScale(30), width: moderateScale(30) }}
                    resizeMode={"contain"}
                  />
                </View>
              </View>
            </View>
            <View
              style={{
                flex: 0.5,
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <View style={Styles.timePersonContainer}>
                <View style={Styles.timeManContainer}>
                  <Image source={Constants.Images.RideInfo.Clock} resizeMode={"contain"} />
                </View>
                <Text style={Styles.buttonText}>
                  {moment(requestTime) == moment().unix()
                    ? Constants.Strings.RideWait.Now
                    : moment(requestTime)
                        .utc()
                        .format("hh:mm A")}
                </Text>
              </View>
              <View style={Styles.timePersonContainer} onPress={() => {}}>
                <View style={Styles.timeManContainer}>
                  <Image source={Constants.Images.RideInfo.Man} resizeMode={"contain"} />
                </View>
                <Text style={Styles.buttonText}>{seatBooked}</Text>
              </View>
              <View style={Styles.bookBtnContainer} onPress={() => {}}>
                <View>
                  <Text style={Styles.WaitText}>{Constants.Strings.RideAccepted.ArivalTime}</Text>
                </View>
                <View style={Styles.waitTime}>
                  <Text numberOfLines={1} style={Styles.bookText}>
                    {/* 10:35 AM */}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            justifyContent: "space-between",
            flexDirection: "row",
            borderColor: Constants.Colors.placehoder,
            borderWidth: 0.4
          }}
        >
          <AuthButton
            buttonStyle={Styles.buttonStyle}
            gradientStyle={Styles.gradientStyle}
            buttonName={Constants.Strings.RideWait.CancelRide}
            textStyle={{ color: Constants.Colors.Primary }}
            onPress={this.cancelRide}
            loading={false}
            gradientColors={["#FFFFFF", "#FFFFFF"]}
          />
          <AuthButton
            buttonStyle={Styles.buttonStyle}
            gradientStyle={Styles.gradientStyle}
            gradientColors={["#F6CF65", "#F6CF65"]}
            buttonName={Constants.Strings.RideWait.ChatWithAdmin}
            onPress={this.moveToChatWindow}
            textStyle={{ color: "#fff" }}
            loading={false}
            // disabled={waitTime.min <= 10 ? true : false}
          />
        </View>
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch)
});
function mapStateToProps(state) {
  return {
    riderLocation: state.riderLocation,
    riderTrip: state.riderTrip
  };
}

reactMixin(RiderRideAccepted.prototype, TimerMixin);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RiderRideAccepted);

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constants.Colors.transparent,
    justifyContent: "flex-end",
    position: "absolute",
    bottom: 0
  },
  modalView: {
    backgroundColor: Constants.Colors.transparent,
    flex: 0.4,
    width: Constants.BaseStyle.DEVICE_WIDTH,
    justifyContent: "space-between",
    //alignItems: "center",
    flexDirection: "column",
    borderRadius: moderateScale(10)
  },
  timePersonContainer: {
    flex: 0.33,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    marginHorizontal: moderateScale(2),
    height: moderateScale(40)
  },
  timeManContainer: {
    backgroundColor: "#A9AFAF",
    borderRadius: moderateScale(100),
    height: moderateScale(20),
    width: moderateScale(20),
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    ...Constants.Fonts.TitilliumWebRegular,
    fontSize: moderateScale(17),
    color: Constants.Colors.Primary
  },
  bookBtnContainer: {
    flex: 0.34,
    flexDirection: "column",
    justifyContent: "flex-start",
    marginHorizontal: moderateScale(2)
  },
  bookBtn: {
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  buttonStyle: { flex: 0.5 },
  gradientStyle: { borderRadius: 0 },
  WaitText: {
    ...Constants.Fonts.TitilliumWebRegular,
    fontSize: moderateScale(17),
    color: Constants.Colors.placehoder,
    textAlign: "left"
  },
  bookText: {
    ...Constants.Fonts.TitilliumWebSemiBold,
    fontSize: moderateScale(18),
    color: Constants.Colors.Black,
    textAlign: "right"
  },
  waitTime: {
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },

  sideMenuImageContainer: {
    paddingHorizontal: moderateScale(10),
    zIndex: 99
  },
  profileImg: {
    height: Constants.BaseStyle.DEVICE_WIDTH * 0.2,
    width: Constants.BaseStyle.DEVICE_WIDTH * 0.2,
    borderColor: Constants.Colors.Primary,
    borderWidth: 0.4,
    borderRadius: moderateScale(100),
    paddingHorizontal: moderateScale(15),
    backgroundColor: Constants.Colors.transparent,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },
  imgAvatar: {
    height: Constants.BaseStyle.DEVICE_WIDTH * 0.18,
    width: Constants.BaseStyle.DEVICE_WIDTH * 0.18
  },
  userInfo: {
    padding: moderateScale(5)
  },
  userName: {
    ...Constants.Fonts.TitilliumWebSemiBold,
    fontSize: moderateScale(19),
    color: Constants.Colors.Primary
  }
});
