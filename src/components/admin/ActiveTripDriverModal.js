/*
Name : Suraj Sanwal 
File Name : ActiveTripDriverModal.js
Description : Contains the ActiveTripDriverModal view.
Date : 27 Nov 2018
*/
import React from "react";
import { View, Image, Text, StyleSheet } from "react-native";

import Constants from "../../constants";
import AuthButton from "../common/AuthButton";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import OnlineDot from "../common/OnlineDot";

const ActiveTripDriverModal = props => {
  let { driver, shuttle, meta, driverTripListing, disabled } = props;
  let { name, profileUrl } = driver;

  return (
    <View style={Styles.container}>
      <View style={Styles.modalView}>
        <View style={Styles.sideMenuImageContainer}>
          <View style={Styles.profileImg}>
            <Image source={{ uri: profileUrl }} style={Styles.imgAvatar} resizeMode={"cover"} />
            <OnlineDot
              size={10}
              active
              dotStyle={{
                bottom: moderateScale(20),
                right: moderateScale(10)
              }}
            />
          </View>
        </View>
        <View
          style={{
            backgroundColor: Constants.Colors.White,
            width: Constants.BaseStyle.DEVICE_WIDTH,
            justifyContent: "space-between",
            //alignItems: "center",
            flexDirection: "column",
            borderTopRightRadius: moderateScale(10),
            borderTopLeftRadius: moderateScale(10)
          }}
        >
          <View
            style={{
              flex: 0.5,
              flexDirection: "row",
              alignItems: "flex-end",
              justifyContent: "space-between"
            }}
          >
            <View style={Styles.timePersonContainer}>
              <Text style={Styles.userName} numberOfLines={1}>
                {name}
              </Text>
            </View>
            <View style={[Styles.timePersonContainer, { flex: 0.77, justifyContent: "flex-end" }]}>
              <Text style={Styles.userName}>{shuttle.name}</Text>
              <View
                style={[
                  Styles.timeManContainer,
                  {
                    height: moderateScale(40),
                    width: moderateScale(40),
                    backgroundColor: Constants.Colors.transparent,
                    borderColor: Constants.Colors.gray,
                    borderWidth: 0.4,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingVertical: moderateScale(5)
                  }
                ]}
              >
                <Image
                  source={Constants.Images.RideInfo.InActiveShuttle}
                  style={{ height: moderateScale(30), width: moderateScale(30) }}
                  resizeMode={"contain"}
                />
              </View>
            </View>
            {/* <View style={Styles.timePersonContainer}>
              <Text style={Styles.userName}>{shuttle.name}</Text>
              <View style={Styles.timeManContainer}>
                <Image source={Constants.Images.Common.Bus} resizeMode={"contain"} />
              </View>
            </View> */}
          </View>
          <View style={{ flex: 0.5, flexDirection: "row", alignItems: "center", justifyContent: "space-between" }}>
            <View style={Styles.timePersonContainer}>
              {/* <View style={Styles.timeManContainer}>
              <Image source={Constants.Images.RideInfo.Clock} resizeMode={"contain"} />
            </View> */}
              <Text style={Styles.buttonText}>{meta && meta.onBoardCount} Passengers</Text>
            </View>
            <View style={Styles.bookBtnContainer}>
              <View style={Styles.waitTime}>
                <Text numberOfLines={1} style={Styles.buttonText}>
                  {meta && meta.newRequestsCount} New Requests
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          justifyContent: "space-between",
          flexDirection: "row",
          borderColor: Constants.Colors.placehoder,
          borderWidth: 0.4
        }}
      >
        <AuthButton
          buttonStyle={Styles.buttonStyle}
          gradientStyle={Styles.gradientStyle}
          buttonName={"Start Chat"}
          textStyle={{ color: Constants.Colors.Primary }}
          onPress={() => alert("underdevelopment")}
          loading={false}
          gradientColors={["#FFFFFF", "#FFFFFF"]}
        />
        <AuthButton
          buttonStyle={Styles.buttonStyle}
          gradientStyle={Styles.gradientStyle}
          gradientColors={["#F6CF65", "#F6CF65"]}
          buttonName={"View Details"}
          onPress={driverTripListing}
          textStyle={{ color: "#fff" }}
          loading={false}
          disabled={disabled}
        />
      </View>
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constants.Colors.transparent,
    justifyContent: "flex-end",
    position: "absolute",
    bottom: 0
  },
  modalView: {
    backgroundColor: Constants.Colors.transparent,
    flex: 0.4,
    width: Constants.BaseStyle.DEVICE_WIDTH,
    justifyContent: "space-between",
    //alignItems: "center",
    flexDirection: "column"
    // borderRadius: moderateScale(10)
  },
  timePersonContainer: {
    flex: 0.33,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    marginHorizontal: moderateScale(2),
    height: moderateScale(40)
  },
  timeManContainer: {
    borderRadius: moderateScale(100),
    height: moderateScale(40),
    width: moderateScale(40),
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0.4,
    borderColor: Constants.Colors.gray,
    overflow: "hidden"
  },
  buttonText: {
    ...Constants.Fonts.TitilliumWebRegular,
    fontSize: moderateScale(17),
    color: Constants.Colors.Primary
  },
  bookBtnContainer: {
    flex: 0.34,
    flexDirection: "column",
    justifyContent: "flex-start",
    marginHorizontal: moderateScale(2)
  },
  bookBtn: {
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  buttonStyle: { flex: 0.5 },
  gradientStyle: { borderRadius: 0 },
  WaitText: {
    ...Constants.Fonts.TitilliumWebRegular,
    fontSize: moderateScale(17),
    color: Constants.Colors.placehoder,
    textAlign: "left"
  },
  bookText: {
    ...Constants.Fonts.TitilliumWebSemiBold,
    fontSize: moderateScale(18),
    color: Constants.Colors.Black,
    textAlign: "right"
  },
  waitTime: {
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },

  sideMenuImageContainer: {
    paddingHorizontal: moderateScale(10),
    zIndex: 99
  },
  profileImg: {
    height: Constants.BaseStyle.DEVICE_WIDTH * 0.2,
    width: Constants.BaseStyle.DEVICE_WIDTH * 0.2,
    borderColor: Constants.Colors.Primary,
    borderWidth: 0.4,
    borderRadius: moderateScale(100),
    paddingHorizontal: moderateScale(15),
    backgroundColor: Constants.Colors.transparent,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },
  imgAvatar: {
    height: Constants.BaseStyle.DEVICE_WIDTH * 0.2,
    width: Constants.BaseStyle.DEVICE_WIDTH * 0.2
  },
  userInfo: {
    padding: moderateScale(5)
  },
  userName: {
    ...Constants.Fonts.TitilliumWebSemiBold,
    fontSize: moderateScale(19),
    color: Constants.Colors.Primary
  }
});

export default ActiveTripDriverModal;
