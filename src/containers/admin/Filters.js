/*
Name : Suraj Sanwal
File Name : Filters.js
Description : Contains the Filters
Date : 23 Nov 2018
*/
import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";
// import moment from "moment";
import Image from "react-native-image-progress";

import Constants from "../../constants";
import Header from "../../components/common/Header";
import * as appActions from "../../actions";
import AuthButton from "../../components/common/AuthButton";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { toastMessage } from "../../config/navigators";
import LocationInput from "../../components/common/LocationInput";
import Online from "../../components/common/OnlineDot";

class Filters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: "driver",
      selectedDrivers: (this.props.listing && this.props.listing.filters && this.props.listing.filters.drivers) || [],
      selectedRideStatus: (this.props.listing && this.props.listing.filters && this.props.listing.filters.status) || [],
      source: (this.props.listing && this.props.listing.filters && this.props.listing.filters.source) || {},
      destination: (this.props.listing && this.props.listing.filters && this.props.listing.filters.destination) || {},
      locationType: Constants.AppCosntants.UserLocation.Source
    };
  }

  static navigatorStyle = {
    navBarHidden: true
  };

  componentDidMount() {
    let { appActions, navigator } = this.props;
    appActions.getDriverListing(1, "", navigator);
  }

  onSelectTerminal = terminal => {
    let { locationType } = this.state;
    if (locationType === Constants.AppCosntants.UserLocation.Source) {
      this.setState({ source: terminal });
    } else {
      this.setState({ destination: terminal });
    }
  };

  onChangeSource = location => {
    let { user, navigator, appActions } = this.props;
    let { source } = this.state;
    if (location === Constants.AppCosntants.UserLocation.Destination && !source._id) {
      toastMessage(navigator, {
        type: Constants.AppCosntants.Notificaitons.Error,
        message: Constants.Strings.Error.SourceNotSelected
      });
      return;
    }
    this.setState({ locationType: location });
    appActions.setLocationType(location, navigator, true, user._id, this.state.source, this.onSelectTerminal);
  };

  clearLocation = location => {
    if (location === Constants.AppCosntants.UserLocation.Source) {
      this.setState({ source: {}, destination: {} });
    } else {
      this.setState({ destination: {} });
    }
  };

  applyFilters = () => {
    let { source, destination, selectedDrivers, selectedRideStatus } = this.state;
    let filter = {
      source,
      destination,
      drivers: selectedDrivers,
      status: selectedRideStatus
    };
    this.props.appActions.updateFilters(filter, this.props.navigator);
  };

  updateSelectedDriver = id => {
    let { selectedDrivers } = { ...this.state };
    if (selectedDrivers.includes(id)) {
      _.remove(selectedDrivers, item => {
        return item === id;
      });
    } else {
      selectedDrivers.push(id);
    }
    this.setState({ selectedDrivers });
  };

  clearFilters = () => {
    this.setState({ selectedRideStatus: [], selectedDrivers: [], source: {}, destination: {} }, () =>
      this.props.appActions.updateFilters({}, this.props.navigator)
    );
  };

  updateRequestStatus = status => {
    let { selectedRideStatus } = { ...this.state };
    if (selectedRideStatus.includes(status)) {
      _.remove(selectedRideStatus, item => {
        return item === status;
      });
    } else {
      selectedRideStatus.push(status);
    }
    this.setState({ selectedRideStatus });
  };
  onFilterChange = filter => {
    let { appActions, navigator } = this.props;
    this.setState(
      {
        filter
      },
      () => {
        if (filter === "driver") {
          appActions.getDriverListing(1, "", navigator);
        }
      }
    );
  };

  renderDriver = () => {
    //let { filter } = this.state;
    const { listing } = this.props;
    const { drivers } = listing;
    let { selectedDrivers } = this.state;
    return drivers.map(driver => {
      let isSelected = selectedDrivers.includes(driver.userIdDriver._id);
      return (
        <TouchableOpacity
          onPress={() => this.updateSelectedDriver(driver.userIdDriver._id)}
          key={driver._id}
          style={{
            paddingHorizontal: moderateScale(25),
            paddingVertical: moderateScale(10),
            borderBottomColor: Constants.Colors.gray,
            borderBottomWidth: 0.4,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text
            style={[
              isSelected ? { ...Constants.Fonts.TitilliumWebSemiBold } : { ...Constants.Fonts.TitilliumWebRegular },
              {
                fontSize: moderateScale(17),
                color: isSelected ? Constants.Colors.Primary : Constants.Colors.gray
              }
            ]}
          >
            {driver.userIdDriver.name}
          </Text>
          <View
            style={{
              height: moderateScale(20),
              width: moderateScale(20),
              borderRadius: moderateScale(100),
              borderColor: Constants.Colors.gray,
              borderWidth: 0.4,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: isSelected ? Constants.Colors.Yellow : Constants.Colors.Transparent
            }}
          >
            {isSelected ? (
              <Image
                source={Constants.Images.Common.Accept}
                style={{ height: moderateScale(18), width: moderateScale(18) }}
                resizeMode={"center"}
              />
            ) : null}
          </View>
        </TouchableOpacity>
      );
    });
  };

  renderOnline = () => {
    return (
      <Online
        size={6}
        dotStyle={{
          bottom: moderateScale(20),
          left: moderateScale(120)
        }}
      />
    );
  };

  renderFilters = () => {
    let { selectedDrivers, filter, selectedRideStatus, source, destination } = this.state;
    let Filters = [
      {
        type: "driver",
        title: "Driver",
        onFilterChange: this.onFilterChange
      },
      {
        type: "passenger",
        title: "Passengers",
        onFilterChange: this.onFilterChange,
        loading: false
      },
      {
        type: "location",
        title: "Location",
        onFilterChange: this.onFilterChange
      }
    ];
    return Filters.map((item, index) => {
      if (filter === item.type) {
        return (
          <TouchableOpacity
            onPress={() => item.onFilterChange(item.type)}
            key={index}
            style={{
              paddingHorizontal: moderateScale(25),
              borderBottomColor: Constants.Colors.Yellow,
              borderBottomWidth: 4,
              paddingVertical: moderateScale(10),
              justifyContent: "space-between",
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                ...Constants.Fonts.TitilliumWebSemiBold,
                fontSize: moderateScale(17),
                color: Constants.Colors.Primary
              }}
            >
              {item.title}
            </Text>
            {item.type === "location" && (source._id || destination._id) ? (
              this.renderOnline()
            ) : (
              <Text
                style={{
                  ...Constants.Fonts.TitilliumWebSemiBold,
                  fontSize: moderateScale(17),
                  color: Constants.Colors.gray
                }}
              >
                {item.type === "driver" && selectedDrivers.length && selectedDrivers.length > 0
                  ? selectedDrivers.length
                  : item.type === "passenger" && selectedRideStatus.length && selectedRideStatus.length > 0
                    ? selectedRideStatus.length
                    : null}
              </Text>
            )}
          </TouchableOpacity>
        );
      } else {
        return (
          <TouchableOpacity
            onPress={() => item.onFilterChange(item.type)}
            key={index}
            style={{
              paddingHorizontal: moderateScale(25),
              paddingVertical: moderateScale(10),
              justifyContent: "space-between",
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                ...Constants.Fonts.TitilliumWebRegular,
                fontSize: moderateScale(17),
                color: Constants.Colors.gray
              }}
            >
              {item.title}
            </Text>
            {item.type === "location" && (source._id || destination._id) ? (
              this.renderOnline()
            ) : (
              <Text
                style={{
                  ...Constants.Fonts.TitilliumWebSemiBold,
                  fontSize: moderateScale(17),
                  color: Constants.Colors.gray
                }}
              >
                {item.type === "driver" && selectedDrivers.length && selectedDrivers.length > 0
                  ? selectedDrivers.length
                  : item.type === "passenger" && selectedRideStatus.length && selectedRideStatus.length > 0
                    ? selectedRideStatus.length
                    : null}
              </Text>
            )}
          </TouchableOpacity>
        );
      }
    });
  };

  renderPassenger = () => {
    let { selectedRideStatus } = this.state;
    let rideStatus = [
      {
        type: Constants.AppCosntants.RideStatus.Request,
        title: "Request"
      },
      {
        type: Constants.AppCosntants.RideStatus.Accepted,
        title: "Accepted"
      },
      {
        type: Constants.AppCosntants.RideStatus.Completed,
        title: "Completed"
      },
      {
        type: Constants.AppCosntants.RideStatus.Cancelled,
        title: "Cancelled"
      },
      {
        type: Constants.AppCosntants.RideStatus.Rejected,
        title: "Rejected"
      },
      {
        type: Constants.AppCosntants.RideStatus.EnRoute,
        title: "EnRoute"
      }
    ];

    return rideStatus.map((status, index) => {
      let isSelected = selectedRideStatus.includes(status.type);
      return (
        <TouchableOpacity
          onPress={() => this.updateRequestStatus(status.type)}
          key={index}
          style={{
            paddingHorizontal: moderateScale(25),
            paddingVertical: moderateScale(10),
            borderBottomColor: Constants.Colors.gray,
            borderBottomWidth: 0.4,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Text
            style={[
              isSelected ? { ...Constants.Fonts.TitilliumWebSemiBold } : { ...Constants.Fonts.TitilliumWebRegular },
              {
                fontSize: moderateScale(17),
                color: isSelected ? Constants.Colors.Primary : Constants.Colors.gray
              }
            ]}
          >
            {status.title}
          </Text>
          <View
            style={{
              height: moderateScale(20),
              width: moderateScale(20),
              borderRadius: moderateScale(100),
              borderColor: Constants.Colors.gray,
              borderWidth: 0.4,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: isSelected ? Constants.Colors.Yellow : Constants.Colors.Transparent
            }}
          >
            {isSelected ? (
              <Image
                source={Constants.Images.Common.Accept}
                style={{ height: moderateScale(18), width: moderateScale(18) }}
                resizeMode={"center"}
              />
            ) : null}
          </View>
        </TouchableOpacity>
      );
    });
  };

  renderLocation = () => {
    let { source, destination } = this.state;
    return (
      <View style={{ marginTop: moderateScale(20) }}>
        <LocationInput
          sourcePlaceholder={Constants.Strings.PlaceHolder.Pickup}
          destinationPlaceholder={Constants.Strings.PlaceHolder.Destination}
          disabledSource={false}
          disabledDestination={false}
          source={source && source.name}
          destination={destination && destination.name}
          onPressSource={() => this.onChangeSource(Constants.AppCosntants.UserLocation.Source)}
          onPressDestination={() => this.onChangeSource(Constants.AppCosntants.UserLocation.Destination)}
          loading={false}
          renderInputBox={false}
          clearBox={this.clearLocation}
        />
      </View>
    );
  };
  render() {
    let { filter } = this.state;
    const { loader } = this.props;
    let { navigator } = this.props;
    return (
      <View style={Styles.container}>
        <Header hideDrawer navigator={navigator} title={"Filter"} color={Constants.Colors.Yellow} />
        <View Opacity style={Styles.filterContainer}>
          <View
            style={{
              flex: 0.4,
              backgroundColor: Constants.Colors.FilterBackground,
              paddingVertical: moderateScale(20)
            }}
          >
            {this.renderFilters()}
          </View>
          <View
            style={{
              flex: 0.6,
              backgroundColor: Constants.Colors.White
            }}
          >
            {filter === "driver" ? (
              loader.driverListing ? (
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                  <ActivityIndicator color={Constants.Colors.Primary} size={"large"} />
                </View>
              ) : (
                this.renderDriver()
              )
            ) : filter === "passenger" ? (
              this.renderPassenger()
            ) : (
              this.renderLocation()
            )}
          </View>
        </View>
        <View
          style={{
            flex: 0.1,
            justifyContent: "space-between",
            flexDirection: "row",
            borderColor: Constants.Colors.placehoder,
            borderWidth: 0.4,
            position: "absolute",
            bottom: 0,
            zIndex: 99
          }}
        >
          <AuthButton
            buttonStyle={Styles.buttonStyle}
            gradientStyle={Styles.gradientStyle}
            buttonName={"Clear"}
            gradientColors={["#FFFFFF", "#FFFFFF"]}
            textStyle={{
              color: Constants.Colors.Primary
            }}
            onPress={this.clearFilters}
            loading={this.props.loader && this.props.loader.changePasswordLoader}
          />
          <AuthButton
            buttonStyle={Styles.buttonStyle}
            gradientStyle={Styles.gradientStyle}
            gradientColors={["#F6CF65", "#F6CF65"]}
            buttonName={"Apply"}
            onPress={() => this.applyFilters()}
            textStyle={{
              color: "#fff"
            }}
            loading={this.props.loader && this.props.loader.changePasswordLoader}
          />
        </View>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    backgroundColor: Constants.Colors.White,
    flex: 1
  },
  filterContainer: {
    flex: 0.9,
    backgroundColor: Constants.Colors.White,
    justifyContent: "flex-start",
    flexDirection: "row"
  },
  buttonStyle: {
    flex: 0.5
  },
  gradientStyle: {
    borderRadius: 0
  }
});

const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch)
});

function mapStateToProps(state) {
  return {
    user: state.user,
    listing: state.listing,
    loader: state.loader
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filters);
