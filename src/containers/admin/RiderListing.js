/*
Name : Suraj Sanwal
File Name : RiderListing.js
Description : Contains the Shuttle Listing
Date : 21 Nov 2018
*/
import React, { Component } from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";
import TimerMixin from "react-timer-mixin";
import reactMixin from "react-mixin";

import Constants from "../../constants";
import Header from "../../components/common/Header";
import * as appActions from "../../actions";
import { handleDeepLink } from "../../config/navigators";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import TerminalListing from "../../components/driver/TerminalListing";
// import Online from "../../components/common/OnlineDot";
class RiderListing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigationEvent);
  }

  static navigatorStyle = {
    navBarHidden: true
  };
  componentDidMount() {
    this.setTimeout(() => {
      this.props.appActions.getRiderListing(1, "", this.props.navigator);
    }, 500);
  }
  onEndReached = () => {
    let { page } = this.state;
    let { listing, appActions } = this.props;
    let { ridersMeta } = listing;
    let { totalNoOfPages } = ridersMeta;
    if (page < totalNoOfPages) {
      page++;
      this.setState({ page: page }, () => {
        appActions.getRiderListing(this.state.page, "", this.props.navigator);
      });
    }
  };

  onRefresh = () => {
    let { appActions } = this.props;
    this.setState({ page: 1 }, () => {
      appActions.getRiderListing(this.state.page, "", this.props.navigator);
    });
  };

  onNavigationEvent = _.debounce(event => {
    handleDeepLink(event, this.props.navigator);
  }, 500);

  updateShuttleStatus() {
    alert("under development");
  }

  renderTrip = ({ item, index }) => {
    let { user } = this.props;
    if (item.rides.length > 0) {
      return (
        <View key={index} style={{ flex: 1 }}>
          <View
            style={{
              paddingVertical: moderateScale(10),
              paddingHorizontal: moderateScale(25),
              justifyContent: "space-between",
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                ...Constants.Fonts.TitilliumWebSemiBold,
                fontSize: moderateScale(17),
                color: Constants.Colors.Primary
              }}
            >
              {item.driver.name}
            </Text>
            <Text
              style={{
                ...Constants.Fonts.TitilliumWebRegular,
                fontSize: moderateScale(17),
                color: Constants.Colors.gray
              }}
            >{`${item.shuttle && item.shuttle.name} ${(item.shuttle && item.shuttle.vechileNo) || "---"}`}</Text>
          </View>
          <TerminalListing hideMeta rides={item.rides} chat={true} userType={user.userType} />
        </View>
      );
    }
  };
  render() {
    let { listing, loader } = this.props;
    // let { filters, riders } = listing;
    let { riders, ridersMeta } = listing;
    return (
      <View style={Styles.mainView}>
        <Header
          navigator={this.props.navigator}
          title={"Passangers"}
          rightIcon={Constants.Images.Common.Chat}
          onRightPress={() => alert("underDevelopment")}
        />
        <View style={{ flex: 1 }}>
          <View style={Styles.noOfRidesView}>
            <Text style={Styles.noOfRidesTxt}>{ridersMeta.currNoOfRecord} Passengers</Text>
          </View>
          {/* <View
            style={{
              flexDirection: "row",
              width: moderateScale(100),
              justifyContent: "space-between",
              alignItems: "center"
              // paddingTop: moderateScale(5)
            }}
          >
            <TouchableOpacity
              style={{
                paddingBottom: moderateScale(15)
              }}
            >
              <Image source={Constants.Images.Common.Sort} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigator.push({ screen: "Filters" })}
              style={{
                flex: 0.52,
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              {filters.destination || filters.source || filters.drivers || filters.status ? (
                <Online color={Constants.Colors.red} size={6} />
              ) : null}
              <View
                style={
                  {
                    // paddingTop: moderateScale(10),
                    // paddingLeft: moderateScale(0)
                  }
                }
              >
                <Image source={Constants.Images.Common.Filter} resizeMode={"contain"} />
              </View>
              <Text style={Styles.dateTxt}>Filter</Text>
            </TouchableOpacity>
          </View> */}
          <FlatList
            data={riders}
            keyExtractor={(item, index) => item._id + index}
            numColumns={1}
            renderItem={this.renderTrip}
            onRefresh={this.onRefresh}
            refreshing={loader.riderListing}
            onEndReached={this.onEndReached}
            onEndReachedThreshold={0}
            style={{ flex: 0.9 }}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch)
});
function mapStateToProps(state) {
  return {
    user: state.user,
    listing: state.listing,
    loader: state.loader
  };
}

reactMixin(RiderListing.prototype, TimerMixin);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RiderListing);

const Styles = StyleSheet.create({
  mainView: { flex: 1, backgroundColor: Constants.Colors.transparent },
  noOfRidesView: {
    flex: 0.05,
    justifyContent: "space-between",
    flexDirection: "row",
    marginHorizontal: moderateScale(25),
    marginVertical: moderateScale(10),
    alignItems: "center"
  },
  rideDateView: {
    flex: 0.2,
    justifyContent: "center",
    marginLeft: Constants.BaseStyle.DEVICE_WIDTH * 0.05
  },
  noOfRidesTxt: {
    color: "#A9AFAF",
    fontSize: moderateScale(17),
    ...Constants.Fonts.TitilliumWebRegular
  },
  dateTxt: {
    color: "#707070",
    fontSize: moderateScale(17),
    ...Constants.Fonts.TitilliumWebSemiBold
  },

  activeBtn: {
    width: moderateScale(100),
    backgroundColor: Constants.Colors.Yellow,
    height: moderateScale(36),
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    borderRadius: moderateScale(3)
  },
  checkBtn: {
    backgroundColor: Constants.Colors.White,
    justifyContent: "center",
    alignItems: "center",
    padding: moderateScale(6),
    margin: moderateScale(3),
    borderRadius: moderateScale(3)
  },
  activeText: {
    ...Constants.Fonts.TitilliumWebSemiBold,
    fontSize: moderateScale(18),
    color: Constants.Colors.White,
    marginHorizontal: moderateScale(5)
  }
});
