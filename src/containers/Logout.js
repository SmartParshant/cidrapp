/*
Name : Suraj Sanwal 
File Name : cancelRide.js
Description : Contains the Logout view.
Date : 2 Nov 2018
*/

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";

import * as appActions from "../actions";
import Constants from "../constants";
import CancelView from "../components/common/CancelView";

class Logout extends Component {
  constructor(props) {
    super(props);
  }
  // static navigatorStyle = {
  //   statusBarColor: "transparent",
  //   navBarHidden: true,
  //   screenBackgroundColor: "transparent",
  //   modalPresentationStyle: "overFullScreen"
  // };
  back = _.debounce(() => {
    this.props.navigator.dismissModal();
  });

  logout = _.debounce(() => {
    this.props.appActions.logout(this.props.navigator);
  });
  render() {
    return (
      <CancelView
        sureMessage={Constants.Strings.CancelRide.AreYouSureYouWantTo}
        cancelMessage={Constants.Strings.CancelRide.Logout}
        onCancelPress={this.back}
        onConfirmPress={this.logout}
      />
    );
  }
}
const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch)
});
function mapStateToProps(state) {
  return {
    loader: state.loder
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Logout);
