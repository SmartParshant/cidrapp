/*
Name : Suraj Sanwal
File Name : RiderRideHistory.js
Description : Contains the Rider history screen
Date : 22 Oct 2018
*/
import React, { Component } from "react";
import { View, Text, Image, StyleSheet, FlatList } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";
import moment from "moment";

import Constants from "../../constants";
import Header from "../../components/common/Header";
import * as appActions from "../../actions";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import { handleDeepLink } from "../../config/navigators";
import constants from "../../constants";

class RiderRideHistory extends Component {
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigationEvent);
    this.state = {
      page: 1
    };
  }
  static navigatorStyle = {
    navBarHidden: true
  };
  componentDidMount() {
    this.fetchRideHistory();
  }

  onEndReached = () => {
    let { page } = this.state;
    let { totalNoOfPages } = this.props.user && this.props.user.history && this.props.user.history.meta;
    if (page < totalNoOfPages) {
      page++;
      this.setState({ page: page }, () => {
        this.fetchRideHistory();
      });
    }
  };

  onRefresh = () => {
    this.setState({ page: 1 }, () => {
      this.fetchRideHistory();
    });
  };
  fetchRideHistory() {
    this.props.appActions.getRideHistory(this.state.page);
  }

  onNavigationEvent = _.debounce(event => {
    handleDeepLink(event, this.props.navigator);
  }, 500);

  renderItem = ({ item }) => {
    let { destLoc, adminId, srcLoc, tripRequestStatus } = item;
    return (
      <View style={Styles.listContainer}>
        <View style={Styles.statusIcon}>
          {tripRequestStatus == constants.AppCosntants.RideStatus.Completed ? (
            <View style={Styles.statusView}>
              <Image source={Constants.Images.Common.Accept} resizeMode={"contain"} />
            </View>
          ) : (
            <View style={Styles.statusViewRejected}>
              <Image source={Constants.Images.Common.Cancel} resizeMode={"contain"} />
            </View>
          )}
        </View>
        <View style={Styles.rideInfo}>
          <Text numberOfLines={1} style={Styles.riderName}>
            {adminId.name}
          </Text>
          <View style={Styles.rideDetails}>
            <View style={Styles.srcDestView}>
              <View style={Styles.srcView}>
                <Image source={Constants.Images.Common.Source} />
                <Text numberOfLines={1} style={Styles.srcText}>
                  {srcLoc.name}
                </Text>
              </View>
              <View style={Styles.srcView}>
                <Image
                  source={Constants.Images.Common.Destination}
                  resizeMode={"contain"}
                  style={{ height: moderateScale(15), width: moderateScale(15) }}
                />
                <Text numberOfLines={1} style={Styles.srcText}>
                  {destLoc.name}
                </Text>
              </View>
            </View>
            <View style={Styles.srcDestView}>
              <View style={Styles.infoView}>
                <Image source={Constants.Images.Common.Source} />
                <Text numberOfLines={1} style={Styles.destText}>
                  {moment(item.requestTime).format("DD MMM, YYYY ")}
                </Text>
              </View>
              <View style={Styles.timeView}>
                <Image source={Constants.Images.RideInfo.ClockGray} />
                <Text numberOfLines={1} style={Styles.destText}>
                  {moment(item.requestTime).format("hh:mm A")}
                </Text>
              </View>
              <View style={Styles.personView}>
                <Image source={Constants.Images.Common.User} />
                <Text numberOfLines={1} style={Styles.srcText}>
                  {item.seatBooked}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    let { navigator, user, loader } = this.props;
    let { history } = user;
    return (
      <View style={Styles.mainView}>
        <Header navigator={navigator} title={"My Trips"} />
        <View style={{ flex: 1 }}>
          {history.rides && history.rides.length ? (
            <View style={Styles.noOfRidesView}>
              <Text style={Styles.noOfRidesTxt}>{history.rides.length} Rides</Text>
              <FlatList
                numColumns={1}
                keyExtractor={item => item._id}
                data={history.rides}
                onRefresh={this.onRefresh}
                refreshing={loader.riderHistory}
                onEndReached={this.onEndReached}
                onEndReachedThreshold={0}
                renderItem={this.renderItem}
                style={{ marginBottom: moderateScale(25) }}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
              />
            </View>
          ) : (
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
              {!loader.riderHistory ? <Text>Ride history not found</Text> : null}
            </View>
          )}
        </View>
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch)
});
function mapStateToProps(state) {
  return {
    user: state.user,
    loader: state.loader
  };
}
const Styles = StyleSheet.create({
  mainView: {
    flex: 1
  },
  noOfRidesView: {
    justifyContent: "center",
    paddingHorizontal: moderateScale(25)
  },
  noOfRidesTxt: {
    paddingTop: moderateScale(10),
    color: "#A9AFAF",
    fontSize: moderateScale(17),
    ...Constants.Fonts.TitilliumWebRegular
  },
  listContainer: {
    flex: 1,
    borderBottomColor: Constants.Colors.placehoder,
    borderBottomWidth: 0.4,
    flexDirection: "row",
    justifyContent: "space-between"
    // paddingHorizontal: moderateScale(25),
    //paddingVertical: moderateScale(15)
  },
  statusIcon: { flex: 0.13, justifyContent: "center", alignItems: "flex-start" },
  rideInfo: {
    flex: 0.87,
    flexDirection: "column",
    paddingVertical: moderateScale(10),
    justifyContent: "flex-start"
  },
  statusView: {
    height: moderateScale(30),
    width: moderateScale(30),
    backgroundColor: Constants.Colors.green,
    borderRadius: moderateScale(100),
    justifyContent: "center",
    alignItems: "center"
  },
  statusViewRejected: {
    height: moderateScale(30),
    width: moderateScale(30),
    backgroundColor: Constants.Colors.red,
    borderRadius: moderateScale(100),
    justifyContent: "center",
    alignItems: "center"
  },
  rideDetails: { flexDirection: "column" },
  srcDestView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  srcView: {
    flex: 0.5,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  infoView: {
    flex: 0.5,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  timeView: {
    flex: 0.4,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  personView: {
    flex: 0.1,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  riderName: {
    ...Constants.Fonts.TitilliumWebSemiBold,
    fontSize: moderateScale(19),
    color: Constants.Colors.Primary
  },
  srcText: {
    ...Constants.Fonts.TitilliumWebRegular,
    fontSize: moderateScale(17),
    color: Constants.Colors.Primary,
    paddingHorizontal: moderateScale(10),
    paddingVertical: moderateScale(3)
  },
  destText: {
    ...Constants.Fonts.TitilliumWebRegular,
    fontSize: moderateScale(17),
    color: Constants.Colors.gray,
    paddingVertical: moderateScale(3),
    paddingHorizontal: moderateScale(10)
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RiderRideHistory);
