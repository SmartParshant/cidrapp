/*
Name : Suraj Sanwal
File Name : DashBoard.js
Description : Contains the Dashboard screen
Date : 17 Sept 2018
*/
import React, { Component } from "react";
import { View, Platform, Image } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Permissions from "react-native-permissions";
import _ from "lodash";
import MapView, { Polyline, Marker } from "react-native-maps";
import TimerMixin from "react-timer-mixin";
import reactMixin from "react-mixin";

import Constants from "../../constants";
import Styles from "../../styles/container/Dashboard";
import Header from "../../components/common/Header";
import * as appActions from "../../actions";
import LocationInput from "../../components/common/LocationInput";
import MapApi from "../../helpers/Maps";
// import { updateLocation, socketInit } from "../../helpers/socket";
import UserSocket from "../../helpers/socket/rider";
import { handleDeepLink, toastMessage } from "../../config/navigators";
import RideInfo from "../../components/rider/RideInfo";
import RideWaitTime from "../../components/rider/RideWaitTime";
import RiderRideAccepted from "../../components/rider/RiderRideAccepted";
import RiderOnShuttle from "../../components/rider/RiderOnShuttle";
import RiderRideCompleted from "../../components/rider/RiderRideCompleted";
import { moderateScale } from "../../helpers/ResponsiveFonts";

class DashBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lastLat: null,
      lastLong: null
    };
  }
  static navigatorStyle = {
    navBarHidden: true
  };
  // static getDerivedStateFromProps(nextProps, prevState) {
  //   if (nextProps.riderTrip && nextProps.riderTrip.shuttleLocation !== prevState.shuttleLocation) {
  //     return {
  //       shuttleLocation: nextProps.riderTrip.shuttleLocation
  //     };
  //   } else return null;
  // }

  componentDidMount() {
    UserSocket.socketInit();
    // socketInit();
    this.checkLocationPermission();
    // this.rateScreenManagement();
  }

  rateScreenManagement = () => {
    let { navigator, riderTrip } = this.props;
    if (riderTrip.rateScreen === Constants.AppCosntants.RideStatus.RatingRide) {
      navigator.push({
        screen: "RiderRating",
        animated: true,
        animationType: "slide-horizontal"
      });
    } else if (riderTrip.rateScreen === Constants.AppCosntants.RideStatus.RatingDriver) {
      navigator.push({
        screen: "RiderRateToDriver",
        animated: true,
        animationType: "slide-horizontal"
      });
    } else {
      return false;
    }
  };
  //conditional popup rendering  accoriding to the ride status
  conditionalViewManagement = () => {
    let { navigator, riderTrip } = this.props;
    if (riderTrip._id) {
      if (riderTrip.tripRequestStatus === Constants.AppCosntants.RideStatus.Request) {
        return <RideWaitTime navigator={navigator} />;
      }
      if (riderTrip.tripRequestStatus === Constants.AppCosntants.RideStatus.Accepted) {
        return <RiderRideAccepted navigator={navigator} />;
      }
      if (riderTrip.tripRequestStatus === Constants.AppCosntants.RideStatus.EnRoute) {
        return <RiderOnShuttle navigator={navigator} />;
      }
      if (riderTrip.tripRequestStatus === Constants.AppCosntants.RideStatus.Completed) {
        return <RiderRideCompleted navigator={navigator} />;
      }
    }
  };
  //check permission for the locations
  checkLocationPermission = async () => {
    let { navigator } = this.props;
    const checkPermission = await Permissions.check("location");
    if (checkPermission == "authorized") {
      this.getAndSetPossitionData();
    } else {
      const requestPermission = await Permissions.request("location");
      if (requestPermission == "authorized") {
        this.getAndSetPossitionData();
      } else {
        toastMessage(navigator, {
          type: Constants.AppCosntants.Notificaitons.Error,
          message: Constants.Strings.Permissions.Locations
        });
      }
    }
  };

  //get location data and set states
  getAndSetPossitionData = () => {
    MapApi.getCurrentPosition(navigator).then(
      region => {
        this.onRegionChange(region);
        // updateLocation(region);
        UserSocket.updateLocation(region);
      },
      () => {
        toastMessage(this.props.navigator, {
          type: Constants.AppCosntants.Notificaitons.Error,
          message: "Please check your location services"
        });
      }
    );
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.riderTrip !== prevState.riderTrip) {
      this.riderMap && this.riderMap.animateToRegion(nextProps.riderTrip.shuttleLocation, 500);
      return null;
    } else return null;
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  onRegionChange = region => {
    this.props.appActions.updateRegion(region);
  };

  onNavigationEvent = _.debounce(event => {
    handleDeepLink(event, this.props.navigator);
  }, 500);

  clearLocation = _.debounce(location => {
    let { riderTrip, navigator, appActions } = this.props;
    if (riderTrip && riderTrip._id) {
      toastMessage(navigator, {
        type: Constants.AppCosntants.Notificaitons.Error,
        message: "Can't change source and destination during active ride!"
      });
      return;
    }
    if (location === Constants.AppCosntants.UserLocation.Source) {
      appActions.setLocationType(Constants.AppCosntants.UserLocation.Destination, navigator, false);
      appActions.setRiderLocation({}, navigator);
    }
    appActions.setLocationType(location, navigator, false);
    appActions.setRiderLocation({}, navigator);
  });

  onChangeSource = _.debounce(location => {
    let { navigator, riderLocation } = this.props;
    let { source } = riderLocation;
    if (location === Constants.AppCosntants.UserLocation.Destination && !source._id) {
      toastMessage(navigator, {
        type: Constants.AppCosntants.Notificaitons.Error,
        message: Constants.Strings.Error.SourceNotSelected
      });
      return;
    }
    this.props.appActions.setLocationType(location, this.props.navigator);
  });

  rightBtnPress = _.debounce(() => {
    this.setTimeout(() => {
      this.props.navigator.pop();
    }, 500);
    this.props.appActions.setLocationType(Constants.AppCosntants.UserLocation.Destination, this.props.navigator, false);
    this.props.appActions.setRiderLocation({}, this.props.navigator);
    this.props.appActions.setLocationType(Constants.AppCosntants.UserLocation.Source, this.props.navigator, false);
    this.props.appActions.setRiderLocation({}, this.props.navigator);

    // this.props.navigator.showModal({
    //   screen: "RideWaitTime",
    // navigatorStyle: {
    //   statusBarColor: "transparent",
    //   navBarHidden: true,
    //   screenBackgroundColor: "transparent",
    //   modalPresentationStyle: "overFullScreen"
    // }
    // });
  });

  renderShuttle = () => {
    let { riderTrip } = this.props;
    let { shuttleLocation } = riderTrip;
    if (shuttleLocation && shuttleLocation.latitude) {
      if (Platform.OS === "android") {
        return (
          <MapView.Marker
            coordinate={shuttleLocation}
            image={Constants.Images.Common.Bus}
            rotation={(shuttleLocation && shuttleLocation.angle) || 0}
          />
        );
      } else {
        return (
          <MapView.Marker coordinate={shuttleLocation}>
            <Image
              style={{
                transform: [{ rotate: `${(shuttleLocation && shuttleLocation.angle) || 0}deg` }]
              }}
              source={Constants.Images.Common.Bus}
            />
          </MapView.Marker>
        );
      }
    } else {
      return null;
    }
  };
  render() {
    let { riderLocation, navigator, riderTrip } = this.props;
    let source = {},
      destination = {},
      userProvider = {};
    if (riderTrip && riderTrip._id) {
      source = riderTrip.srcLoc;
      destination = riderTrip.destLoc;
      userProvider = riderTrip.userProvider;
    } else {
      source = riderLocation.source;
      destination = riderLocation.destination;
      userProvider = riderLocation.userProvider;
    }
    let { region } = riderTrip;
    return (
      <View style={Styles.mainView}>
        <KeyboardAwareScrollView style={Styles.container} scrollEnabled={false}>
          <Header
            headerText={{ color: Constants.Colors.Primary }}
            navigator={navigator}
            color={Constants.Colors.transparent}
            title={userProvider && userProvider.name}
            rightIcon={Constants.Images.Common.Edit}
            onRightPress={!riderTrip._id ? () => this.rightBtnPress() : () => {}}
          />
          <View style={Styles.keyboardScroll}>
            <View style={Styles.wrapper}>
              <LocationInput
                sourcePlaceholder={Constants.Strings.PlaceHolder.Pickup}
                destinationPlaceholder={Constants.Strings.PlaceHolder.Destination}
                disabledSource={riderTrip && riderTrip._id}
                disabledDestination={riderTrip && riderTrip._id}
                source={source && source.name}
                destination={destination && destination.name}
                onPressSource={this.onChangeSource}
                onPressDestination={this.onChangeSource}
                loading={false}
                renderInputBox={false}
                clearBox={this.clearLocation}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
        {!(riderTrip, riderTrip._id) && (source && source._id) && (destination && destination._id) ? (
          <View
            style={{
              position: "absolute",
              zIndex: 99,
              bottom: 0,
              backgroundColor: Constants.Colors.White,
              borderRadius: moderateScale(10)
            }}
          >
            <RideInfo navigator={navigator} />
          </View>
        ) : null}
        <MapView
          ref={refs => (this.riderMap = refs)}
          followsUserLocation={true}
          zoomEnabled={true}
          rotateEnabled={true}
          scrollEnabled={true}
          // onRegionChange={this.onRegionChange}
          //  showsUserLocation={true}
          region={region && region.latitude ? region : null}
          loadingEnabled={true}
          //initialRegion={region}
          style={{
            height: Constants.BaseStyle.DEVICE_HEIGHT,
            width: Constants.BaseStyle.DEVICE_WIDTH
          }}
        >
          {source && source.loc && destination && destination.loc ? (
            <Polyline
              coordinates={[
                { longitude: source.loc[0], latitude: source.loc[1] },
                { longitude: destination.loc[0], latitude: destination.loc[1] }
              ]}
              strokeWidth={5}
              strokeColor={Constants.Colors.Primary}
            />
          ) : null}
          {this.renderShuttle()}
          {source && source.loc ? (
            <Marker.Animated
              coordinate={{ longitude: source.loc[0], latitude: source.loc[1] }}
              image={Constants.Images.Common.Source}
            />
          ) : null}
          {destination && destination.loc ? (
            <Marker.Animated
              coordinate={{ longitude: destination.loc[0], latitude: destination.loc[1] }}
              image={Constants.Images.Common.Destination}
            />
          ) : null}
        </MapView>
        {this.conditionalViewManagement()}
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch)
});
function mapStateToProps(state) {
  return {
    user: state.user,
    riderLocation: state.riderLocation,
    riderTrip: state.riderTrip,
    app: state.app
  };
}

reactMixin(DashBoard.prototype, TimerMixin);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashBoard);

{
  /* */
}
