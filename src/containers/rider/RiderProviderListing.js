/*
Name : Suraj Sanwal
File Name : RiderProviderListing.js
Description : Contains the Provider listing
Date : 11 Oct 2018
*/

import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, FlatList, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";

import * as appActions from "../../actions";
import Constants from "../../constants";
import Header from "../../components/common/Header";
import Styles from "../../styles/container/RiderProviderListing";
import { handleDeepLink } from "../../config/navigators";

class RiderProviderListing extends Component {
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigationEvent);
    this.state = {
      Provider: {}
    };
  }
  static navigatorStyle = {
    navBarHidden: true
  };

  componentDidMount() {
    let { navigator, appActions } = this.props;
    navigator.setDrawerEnabled({
      side: "left",
      enabled: true
    });
    this.conditionalBaseRendering();
    appActions.getServiceProviders("", navigator);
    appActions.getRideData(navigator);
  }
  conditionalBaseRendering = () => {
    let { riderTrip, navigator } = this.props;
    if (riderTrip._id) {
      if (riderTrip.rateScreen === Constants.AppCosntants.RideStatus.RatingRide) {
        navigator.push({
          screen: "RiderRating"
        });
      } else if (riderTrip.rateScreen === Constants.AppCosntants.RideStatus.RatingDriver) {
        navigator.push({
          screen: "RiderRateToDriver"
        });
      } else {
        navigator.resetTo({
          screen: "DashBoard"
        });
      }
    }
  };
  onNavigationEvent = _.debounce(event => {
    handleDeepLink(event, this.props.navigator);
  }, 500);
  onProviderPress = _.debounce(provider => {
    this.props.appActions.setProvider(provider, this.props.navigator);
  });

  onCanclePress = _.debounce(() => {
    this.setState({ Provider: {} });
  });

  onRightPress = _.debounce(() => {
    this.props.navigator.showModal({
      screen: "ProviderSearchListing",
      animationType: "slide-up",
      passProps: {
        onProviderPress: this.onProviderPress
      }
      // navigatorStyle: {
      //   statusBarColor: "transparent",
      //   navBarHidden: false,
      //   screenBackgroundColor: "transparent",
      //   modalPresentationStyle: "overFullScreen"
      // }
    });
  });
  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.onProviderPress(item)} key={item._id} style={Styles.itemContaier}>
        <View style={Styles.imageContainer}>
          <Image source={Constants.Images.Common.Provider} resizeMode={"contain"} style={Styles.shuttleImg} />
        </View>
        <View style={Styles.textContainer}>
          <Text style={Styles.titleText}>{item.name}</Text>
        </View>
        {/* {Provider._id == item._id ? (
          <View style={Styles.yellowBtn}>
            <Image source={Constants.Images.Common.Accept} resizeMode={"contain"} />
          </View>
        ) : null} */}
      </TouchableOpacity>
    );
  };

  render() {
    let { loader, navigator, riderLocation } = this.props;
    let { providers } = riderLocation;
    return (
      <View style={Styles.container}>
        <Header
          color={Constants.Colors.Yellow}
          navigator={navigator}
          title={"Select Provider"}
          rightIcon={Constants.Images.Common.Search}
          onRightPress={this.onRightPress}
        />

        {loader.providerList ? (
          <ActivityIndicator size="large" color={Constants.Colors.White} />
        ) : providers.length > 0 ? (
          <View style={Styles.shuttleContainer}>
            <Text style={Styles.textStyle}>{providers.length} Shuttle Provider</Text>
            <FlatList
              data={providers}
              renderItem={this.renderItem}
              numColumns={1}
              keyExtractor={item => item._id}
              style={Styles.listStyle}
              extraData={this.state.myShuttle}
            />
          </View>
        ) : (
          <View style={Styles.notFound}>
            <Text style={Styles.titleText}>No Shuttle Provider Found</Text>
          </View>
        )
        /*(
         
        )} */
        }
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch)
});
function mapStateToProps(state) {
  return {
    user: state.user,
    loader: state.loader,
    riderLocation: state.riderLocation,
    riderTrip: state.riderTrip
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RiderProviderListing);
