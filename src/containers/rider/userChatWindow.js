/*
Name : Suraj Sanwal 
File Name : userChatWindow.js
Description : Contains the Chat view for rider.
Date : 05 oct 2018
*/
import React, { Component } from "react";
import { View, Image } from "react-native";
import { GiftedChat, Send } from "react-native-gifted-chat";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import * as appActions from "../../actions";
import Header from "../../components/common/Header";
import Constants from "../../constants";
import LocationInput from "../../components/common/LocationInput";
import { moderateScale } from "../../helpers/ResponsiveFonts";

class ChatWindow extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    messages: [],
    loadEarlier: true,
    typingText: null,
    isLoadingEarlier: false
  };
  static navigatorStyle = {
    navBarHidden: true
  };

  onBackPress = () => {
    this.props.navigator.pop();
  };

  UNSAFE_componentWillMount() {
    this._isMounted = true;
    this.onLoadEarlier();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidMount = () => {
    this.props.navigator.setOnNavigatorEvent(this.onNavigationEvent);
  };
  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages)
    }));
  }
  //   onSend(msg) {
  //     const { ChatUser, ChatRoomId } = this.props.navigation;
  //     const { _id } = this.props.userData;
  //     const toId = ChatUser._id;
  //     this.setState(previousState => ({
  //       messages: GiftedChat.append(previousState.messages, msg)
  //     }));
  //     let messageJson = {
  //       _id: Math.round(Math.random() * 1000000),
  //       text: msg[0].text,
  //       createdAt: msg[0].createdAt,
  //       to: toId,
  //       from: _id,
  //       chatroomId: ChatRoomId
  //     };
  //     this.socket.emit("save-message", messageJson);
  //   }
  onReceivedMessage = () => {
    // onReceivedMessage = messages => {
    // const { ChatUser, ChatRoomId } = this.props.navigation;
    // const { _id } = this.props.user;
    // if (this.props.userData && messages.from !== this.props.userData._id && messages.chatroomId === ChatRoomId) {
    //   this.setState(previousState => {
    //     return {
    //       messages: GiftedChat.append(previousState.messages, {
    //         _id: Math.round(Math.random() * 1000000),
    //         text: messages.text,
    //         createdAt: new Date(),
    //         user: ChatUser
    //       })
    //     };
    //   });
    // }
  };
  onLoadEarlier() {
    // const { ChatRoomId } = this.props.navigation;
    // const { _id } = this.props.userData;
    // this.setState(previousState => {
    //   return {
    //     isLoadingEarlier: true
    //   };
    // });
    // const dataToSend = {
    //   chatroom: ChatRoomId,
    //   userId: _id
    // };
    // this.props.getChatHistory(dataToSend, this.props.lang, res => {
    //   if (this._isMounted === true) {
    //     this.setState(previousState => {
    //       return {
    //         messages: GiftedChat.prepend(previousState.messages, res.data),
    //         loadEarlier: false,
    //         isLoadingEarlier: false
    //       };
    //     });
    //   }
    // });
  }
  renderSend = sendProps => {
    if (sendProps.text.trim().length > 0) {
      return (
        <Send {...sendProps}>
          <View
            style={{
              backgroundColor: Constants.Colors.Yellow,
              height: moderateScale(35),
              width: moderateScale(35),
              alignSelf: "center",
              padding: moderateScale(10),
              borderRadius: moderateScale(100),
              justifyContent: "center",
              alignItems: "center",
              bottom: moderateScale(3)
            }}
          >
            <Image source={Constants.Images.Common.Accept} resizeMode={"contain"} />
          </View>
        </Send>
      );
    }
    return null;
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header hideDrawer navigator={this.props.navigator} title={"Chat with Admin"} onBackPress={this.onBackPress} />
        {/* <ScrollView
          scrollEnabled={true}
          style={{flex:0.5,borderColor:'red',borderWidth:1,width:Constants.BaseStyle.DEVICE_WIDTH }}
        > */}
        <View style={{ padding: moderateScale(20) }}>
          <LocationInput editable={false} source={"Terminal-1, Airport Pkwy"} destination={"Courtyard by Marriott"} />
        </View>
        <GiftedChat
          minInputToolbarHeight={80}
          messages={this.state.messages}
          keyboardShouldPersistTaps={"always"}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1
          }}
          loadEarlier={this.state.loadEarlier}
          isLoadingEarlier={this.state.isLoadingEarlier}
          onLoadEarlier={this.onLoadEarlier}
          alwaysShowSend={true}
          renderSend={this.renderSend}
        />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch)
});
function mapStateToProps(state) {
  return {
    user: state.user,
    loader: state.loader
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatWindow);
