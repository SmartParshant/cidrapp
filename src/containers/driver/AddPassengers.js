/*
Name : Amit Singh
File Name : AddPassengers.js
Description : Contains the Add Passengers screen
Date : 17 Sept 2018
*/
import React, { Component } from "react";
import { View, StyleSheet, Text, Image } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";

import Constants from "../../constants";
import Header from "../../components/common/Header";
import * as appActions from "../../actions";
import { moderateScale } from "../../helpers/ResponsiveFonts";
import FloatingInput from "../../components/common/FloatingInput";
import AuthButton from "../../components/common/AuthButton";

class AddPassengers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Terminal 1 Passengers",
      from: "Courtyar by Marriott",
      to: "Terminal-1, Airport Lax",
      noOfPassengers: "6"
    };
  }
  static navigatorStyle = {
    navBarHidden: true
  };
  componentDidMount() {
    this.props.navigator.setOnNavigatorEvent(this.onNavigationEvent);
    this.props.navigator.setStyle({
      statusBarColor: Constants.Colors.Yellow
    });
  }
  onNavigationEvent = _.debounce(event => {
    if (event.type == "DeepLink") {
      this.props.navigator.resetTo({
        screen: event.link,
        animated: true,
        animationType: "slide-horizontal"
      });
    }
  }, 500);

  drawerPress() {
    this.props.navigator.toggleDrawer({
      side: "left"
    });
  }

  render() {
    const { bottomBtnView, headerTxt } = Styles;
    return (
      <View style={{ flex: 1 }}>
        <Header onDrawerPress={() => this.drawerPress()} title={"Add Passengers"} />
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            marginHorizontal: Constants.BaseStyle.DEVICE_WIDTH * 0.08
          }}
        >
          <View
            style={{
              flex: 0.1,
              justifyContent: "center"
            }}
          >
            <Text style={headerTxt}>Please add passengers detail below</Text>
          </View>
          <View style={{ flex: 0.1 }}>
            <FloatingInput
              label={"Name"}
              onChangeText={name => {
                this.setState({ name });
              }}
              value={this.state.name}
              returnKeyType={"next"}
              autoCapitalize={"none"}
              ref={ref => (this.name = ref)}
              onSubmitEditing={() => {
                this.focusNext("from");
              }}
            />
          </View>
          <View
            style={{
              flex: 0.3,
              flexDirection: "row",
              alignItems: "center"
            }}
          >
            <View style={{ flex: 0.1 }}>
              <Image source={Constants.Images.Dashboard.PicupLocation} />
            </View>
            <View style={{ flex: 0.9 }}>
              <FloatingInput
                label={"From"}
                onChangeText={from => {
                  this.setState({ from });
                }}
                value={this.state.from}
                returnKeyType={"next"}
                autoCapitalize={"none"}
                ref={ref => (this.from = ref)}
                onSubmitEditing={() => {
                  this.focusNext("to");
                }}
              />
              <FloatingInput
                label={"To"}
                onChangeText={to => {
                  this.setState({ to });
                }}
                value={this.state.to}
                returnKeyType={"next"}
                autoCapitalize={"none"}
                ref={ref => (this.to = ref)}
                onSubmitEditing={() => {
                  this.focusNext("noOfPassengers");
                }}
              />
            </View>
          </View>
          <View style={{ flex: 0.2 }}>
            <FloatingInput
              label={"Number of Passengers"}
              onChangeText={noOfPassengers => {
                this.setState({ noOfPassengers });
              }}
              value={this.state.noOfPassengers}
              returnKeyType={"next"}
              autoCapitalize={"none"}
              ref={ref => (this.noOfPassengers = ref)}
              onSubmitEditing={() => {
                this.focusNext("from");
              }}
            />
          </View>
        </View>
        <View style={bottomBtnView}>
          <View style={{ flex: 0.5 }}>
            <AuthButton
              buttonStyle={Styles.buttonStyle}
              gradientStyle={Styles.gradientStyle}
              gradientColors={[Constants.Colors.White, Constants.Colors.White]}
              buttonName={"Cancel"}
              onPress={() => alert("Cancel")}
              textStyle={{
                color: Constants.Colors.placehoder,
                fontSize: moderateScale(18)
              }}
              loading={this.props.loader}
            />
          </View>
          <View style={{ flex: 0.5 }}>
            <AuthButton
              buttonStyle={Styles.buttonStyle}
              gradientStyle={Styles.gradientStyle}
              gradientColors={[Constants.Colors.Yellow, Constants.Colors.Yellow]}
              buttonName={"Add"}
              onPress={() => alert("Add")}
              textStyle={{
                color: Constants.Colors.White,
                fontSize: moderateScale(18)
              }}
              loading={this.props.loader}
            />
          </View>
        </View>
      </View>
    );
  }
}
const mapdestinationToProps = destination => ({
  appActions: bindActionCreators(appActions, destination)
});
function mapStateToProps(state) {
  return {
    user: state.user,
    riderLocation: state.riderLocation
  };
}
export default connect(
  mapStateToProps,
  mapdestinationToProps
)(AddPassengers);

const Styles = StyleSheet.create({
  gradientStyle: {
    borderRadius: moderateScale(0)
  },
  bottomBtnView: {
    flex: 0.2,
    flexDirection: "row",
    position: "absolute",
    left: 0,
    bottom: 0
  },
  headerTxt: {
    color: Constants.Colors.gray,
    fontSize: moderateScale(17),
    ...Constants.Fonts.TitilliumWebRegular
  }
});
