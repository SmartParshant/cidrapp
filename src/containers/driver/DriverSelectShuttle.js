/*
Name : Suraj Sanwal
File Name : SelectShuttle.js
Description : Contains the Select shuttle screen
Date : 25 Sept 2018
*/

import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, FlatList, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";

import * as appActions from "../../actions";
import Constants from "../../constants";
import Styles from "../../styles/container/Driver.Selectshuttle";
import Header from "../../components/common/Header";
import AuthButton from "../../components/common/AuthButton";
import { handleDeepLink } from "../../config/navigators";

class SelectShuttle extends Component {
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigationEvent);
    this.state = {
      myShuttle: {}
    };
  }
  static navigatorStyle = {
    navBarHidden: true
  };
  componentDidMount() {
    let { navigator, appActions } = this.props;
    appActions.getDriverShuttle(navigator);
  }
  drawerPress() {
    this.props.navigator.toggleDrawer({
      side: "left"
    });
  }
  onNavigationEvent = _.debounce(event => {
    handleDeepLink(event, this.props.navigator);
  }, 500);

  onShuttlePress = _.debounce(shuttle => {
    this.setState({ myShuttle: shuttle });
  });

  onCanclePress = _.debounce(() => {
    this.setState({ myShuttle: {} });
  });

  onShuttleActive = _.debounce(() => {
    let { myShuttle } = this.state;
    let { user, navigator, appActions } = this.props;
    let { _id } = user;
    let data = {
      shuttle: myShuttle,
      tripId: "",
      status: true,
      driverId: _id
    };
    appActions.updateTripStatus(data, navigator);
  });

  renderItem = ({ item }) => {
    let { myShuttle } = this.state;
    return (
      <TouchableOpacity onPress={() => this.onShuttlePress(item)} key={item._id} style={Styles.itemContaier}>
        <View style={Styles.imageContainer}>
          <Image source={Constants.Images.Common.Admin} resizeMode={"contain"} style={Styles.shuttleImg} />
        </View>
        <View style={Styles.textContainer}>
          <Text style={Styles.titleText}>{item.carModel}</Text>
          <Text style={Styles.subText}>{item.company}</Text>
        </View>
        {myShuttle._id == item._id ? (
          <View style={Styles.yellowBtn}>
            <Image source={Constants.Images.Common.Accept} resizeMode={"contain"} />
          </View>
        ) : null}
      </TouchableOpacity>
    );
  };

  render() {
    let { loader, navigator, shuttle } = this.props;
    let { myShuttle } = this.state;
    return (
      <View style={Styles.container}>
        <Header color={Constants.Colors.Yellow} navigator={navigator} title={"Select Shuttle"} />
        {myShuttle._id ? (
          <View style={Styles.wraper}>
            <AuthButton
              buttonStyle={Styles.buttonStyle}
              gradientStyle={Styles.gradientStyle}
              buttonName={"Cancel"}
              gradientColors={["#FFFFFF", "#FFFFFF"]}
              textStyle={{ color: Constants.Colors.Primary }}
              onPress={() => this.onCanclePress()}
              //loading={this.props.loader && this.props.loader.changePasswordLoader}
            />
            <AuthButton
              buttonStyle={Styles.buttonStyle}
              gradientStyle={Styles.gradientStyle}
              gradientColors={["#F6CF65", "#F6CF65"]}
              buttonName={"Activate"}
              textStyle={{ color: "#fff" }}
              icon={Constants.Images.Common.Accept}
              onPress={() => this.onShuttleActive()}
              //loading={this.props.loader && this.props.loader.changePasswordLoader}
            />
          </View>
        ) : null}

        {loader.shuttleList ? (
          <ActivityIndicator size="large" color={Constants.Colors.White} />
        ) : shuttle.shuttles && shuttle.shuttles.length > 0 ? (
          <View style={Styles.shuttleContainer}>
            <Text style={Styles.textStyle}>{shuttle.shuttles && shuttle.shuttles.length} Shuttle Available</Text>
            <FlatList
              data={shuttle.shuttles}
              renderItem={this.renderItem}
              numColumns={1}
              keyExtractor={item => item._id}
              style={Styles.listStyle}
              extraData={this.state.myShuttle}
            />
          </View>
        ) : (
          <View style={Styles.notFound}>
            <Text style={Styles.titleText}>No Shuttle Found</Text>
          </View>
        )
        /*(
         
        )} */
        }
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch)
});
function mapStateToProps(state) {
  return {
    user: state.user,
    loader: state.loader,
    shuttle: state.shuttle,
    trip: state.trip
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectShuttle);
