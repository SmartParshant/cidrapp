/*
Name : Suraj Sanwal
File Name : Maps.js
Description : Contains the Maps View
Date : 12 OCT 2018
*/

import React, { Component } from "react";
import { View, StyleSheet, Image, Text, TouchableOpacity, Platform } from "react-native";
import MapView, { Polyline, Marker, Callout } from "react-native-maps";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";
import Permissions from "react-native-permissions";
import TimerMixin from "react-timer-mixin";
import reactMixin from "react-mixin";

import MapApi from "../../helpers/Maps";
import * as appActions from "../../actions";
import Constants from "../../constants";
import Header from "../../components/common/Header";
import CustomCallOut from "../../components/driver/CustomCallOut";
import RideStatusModal from "../../components/driver/RideStatusModal";
import { moderateScale } from "../../helpers/ResponsiveFonts";
// import { socketInit } from "../../helpers/socket";
import DriverSocket from "../../helpers/socket/driver";
import { handleDeepLink, toastMessage } from "../../config/navigators";

class Maps extends Component {
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigationEvent);
    this.state = {
      shuttleRoutes: [],
      routeCoordinates: [],
      distanceTravelled: 0
    };
  }
  static navigatorStyle = {
    navBarHidden: true
  };

  componentDidMount() {
    let { appActions } = this.props;
    DriverSocket.socketInit();
    this.checkLocationPermission();
    appActions.getRideRequests(this.props.navigator);
    this.getDriverRoute();
  }

  //check permission for the locations
  checkLocationPermission = async () => {
    let { navigator } = this.props;
    const requestPermission = await Permissions.request("location");
    if (requestPermission == "authorized") {
      this.getAndSetPossitionData();
    } else {
      toastMessage(navigator, {
        type: Constants.AppCosntants.Notificaitons.Error,
        message: Constants.Strings.Permissions.Locations
      });
    }
    // }
  };

  //get location data and set states
  getAndSetPossitionData = () => {
    MapApi.watcher().then(
      region => {
        this.onRegionChange(region);
        // updateLocation(region);
      },
      () => {
        toastMessage(this.props.navigator, {
          type: Constants.AppCosntants.Notificaitons.Error,
          message: "Please check your location services"
        });
      }
    );
  };

  getDriverRoute = () => {
    let { trip } = this.props;
    let { driverRoute } = trip;
    let shuttleRoute = [];
    driverRoute &&
      driverRoute.length &&
      driverRoute.map(item => {
        shuttleRoute.push({
          longitude: item.loc[0],
          latitude: item.loc[1]
        });
      });

    this.setTimeout(() => {
      if (shuttleRoute.length > 1) {
        MapApi.getRoutePoints(shuttleRoute).then(shuttleRoutes => {
          this.setState({ shuttleRoutes });
          MapApi.getRegionForCoordinates(shuttleRoutes).then(region => {
            let regiondata = { ...region, angle: 0 };
            this.onRegionChange(regiondata);
          });
        });
      }
    }, 1000);
  };

  componentWillUnmount() {}
  // calcDistance = newLatLng => {
  //   const { prevLatLng } = this.state;
  //   return haversine(prevLatLng, newLatLng) || 0;
  // };

  onRegionChange = region => {
    this.props.appActions.updateRegion(region);
  };

  loadPassengerModal = _.debounce(() => {
    this.props.navigator.showModal({
      screen: "PassengerModal",
      animationType: "slide-up",
      navigatorStyle: {
        statusBarColor: "transparent",
        navBarHidden: true,
        screenBackgroundColor: "transparent",
        modalPresentationStyle: "overFullScreen"
      }
    });
  });

  onTerminalPress = _.debounce(terminal => {
    this.props.navigator.push({
      screen: "TerminalDetails",
      passProps: { terminal },
      animated: true,
      animationType: "slide-horizontal"
    });
  });

  onNavigationEvent = _.debounce(event => {
    handleDeepLink(event, this.props.navigator);
  }, 500);

  renderShuttle = () => {
    /* function will display shuttle */
    let { trip } = this.props;
    let { region } = trip;
    if (region && region.latitude) {
      if (Platform.OS === "android") {
        return (
          <MapView.Marker
            coordinate={region}
            image={Constants.Images.Common.Bus}
            rotation={(region && region.angle) || 0}
          />
        );
      } else {
        return (
          <MapView.Marker coordinate={region}>
            <Image
              style={{
                transform: [{ rotate: `${(region && region.angle) || 0}deg` }]
              }}
              source={Constants.Images.Common.Bus}
            />
          </MapView.Marker>
        );
      }
    } else {
      return null;
    }
  };

  currentTerminalModal = () => {
    let { trip } = this.props;
    let { currentTerminal } = trip;
    /* method renderd whenever driver has riders on terminal to complete ride as well for continue ride*/
    if ((currentTerminal && currentTerminal.isContinueModal) || (currentTerminal && currentTerminal.isCompleteModal)) {
      return <RideStatusModal navigator={this.props.navigator} />;
    }
  };

  renderRequestModal = () => {
    let { trip } = this.props;
    let { rides, meta } = trip;
    if (rides && rides.length && meta.newRequestsCount > 0) {
      return (
        <TouchableOpacity
          onPress={this.loadPassengerModal}
          style={{
            position: "absolute",
            zIndex: 999,
            backgroundColor: Constants.Colors.White,
            width: Constants.BaseStyle.DEVICE_WIDTH,
            bottom: 0,
            flexDirection: "row",
            justifyContent: "space-around",
            alignItems: "center",
            height: Constants.BaseStyle.DEVICE_HEIGHT * 0.1,
            borderTopLeftRadius: moderateScale(20),
            borderTopRightRadius: moderateScale(20)
          }}
        >
          <View
            style={{
              height: moderateScale(40),
              width: moderateScale(40),
              justifyContent: "center",
              alignItems: "center",
              padding: moderateScale(20)
            }}
          >
            <Image source={Constants.Images.Common.UpArrow} />
          </View>
          <View style={{ flex: 0.85 }}>
            <Text
              numberOfLines={1}
              style={{
                ...Constants.Fonts.TitilliumWebSemiBold,
                fontSize: moderateScale(19),
                color: Constants.Colors.Black
              }}
            >
              {meta && meta.newRequestsCount} New Request
            </Text>
            <Text
              numberOfLines={1}
              style={{
                ...Constants.Fonts.TitilliumWebRegular,
                fontSize: moderateScale(17),
                color: Constants.Colors.placehoder
              }}
            >
              Lax Terminal 1, Lax Terminal 1
            </Text>
          </View>
          <View style={{ padding: moderateScale(20) }}>
            <Text
              numberOfLines={1}
              style={{
                ...Constants.Fonts.TitilliumWebSemiBold,
                fontSize: moderateScale(16),
                color: Constants.Colors.placehoder
              }}
            >
              View All
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
  };

  renderTerminalsCallouts = () => {
    let { trip } = this.props;
    let { driverRoute, rides } = trip;
    let terminals = [];
    if (driverRoute && driverRoute.length > 0) {
      driverRoute.map(item => {
        let terminalInfo = { ...item };
        terminalInfo.newRequestsCount = 0;
        terminalInfo.onBoardCount = 0;
        terminalInfo.image =
          item.type === "startTerminal"
            ? Constants.Images.Common.Source
            : item.type === "endTerminal"
              ? Constants.Images.Common.Destination
              : Constants.Images.Common.Source;
        rides.map(ride => {
          if (ride.srcLoc && ride.srcLoc._id === item._id) {
            if (ride.tripRequestStatus === Constants.AppCosntants.RideStatus.Request) {
              terminalInfo.newRequestsCount += 1;
            }
            if (ride.tripRequestStatus === Constants.AppCosntants.RideStatus.EnRoute) {
              terminalInfo.onBoardCount += 1;
            }
          }
        });
        terminals.push(terminalInfo);
      });
    }
    return terminals.map((terminal, index) => {
      return (
        <Marker.Animated
          coordinate={{
            longitude: terminal.loc[0],
            latitude: terminal.loc[1]
          }}
          title={terminal.name && terminal.name.trim()}
          image={terminal.image}
          key={index}
        >
          <Callout
            onPress={() => this.onTerminalPress(terminal)}
            tooltip={true}
            style={{ alignItems: "center", padding: moderateScale(10) }}
          >
            <CustomCallOut terminal={terminal} onTerminalPress={this.onTerminalPress} />
          </Callout>
        </Marker.Animated>
      );
    });
  };

  render() {
    let { trip } = this.props;
    let { region } = trip;
    let { shuttleRoutes } = this.state;
    return (
      <View style={styles.container}>
        <View
          style={{
            position: "absolute",
            zIndex: 999,
            backgroundColor: Constants.Colors.transparent,
            width: Constants.BaseStyle.DEVICE_WIDTH
          }}
        >
          <Header color={Constants.Colors.transparent} navigator={this.props.navigator} />
        </View>
        {this.currentTerminalModal()}

        {this.renderRequestModal()}
        <MapView
          followsUserLocation={true}
          showsUserLocation={false}
          zoomEnabled={true}
          rotateEnabled={true}
          scrollEnabled={true}
          region={region && region.latitude ? region : null}
          loadingEnabled={true}
          style={{
            height: Constants.BaseStyle.DEVICE_HEIGHT,
            width: Constants.BaseStyle.DEVICE_WIDTH
          }}
        >
          <Polyline coordinates={shuttleRoutes} strokeWidth={4} strokeColor={"#333"} />
          {this.renderShuttle()}
          {this.renderTerminalsCallouts()}
        </MapView>
      </View>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  appActions: bindActionCreators(appActions, dispatch)
});
function mapStateToProps(state) {
  return {
    user: state.user,
    riderLocation: state.riderLocation,
    trip: state.trip,
    loader: state.loader,
    app: state.app
  };
}

reactMixin(Maps.prototype, TimerMixin);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Maps);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constants.Colors.transparent
  },
  map: {
    height: Constants.BaseStyle.DEVICE_HEIGHT
  },
  bubble: {
    flex: 1,
    backgroundColor: Constants.Colors.transparent
  }
});
