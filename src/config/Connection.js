"use strict";
/*
 * @file: Connection.js
 * @description: Connection file for the application
 * @date: 22.03.2018
 * @author: Gurtej Singh
 * */
/* eslint-disable */
const localhost = {
  anand: "172.24.4.48:3010",
  ranjeet: "172.24.4.129:3010",
  amit: "172.24.5.156:3010"
};
const frontEndLocal = "172.24.4.48:4200";
const frontEndStaging = "stagingsdei.com:4201";
const staging = "stagingsdei.com:4202";
const live = "176.126.246.61";
const apiPath = "api";

//uncomment these four line for use localhost

const running_url = localhost.anand,
  frontEndUrl = `http://${frontEndLocal}`,
  http_url = `http://${running_url}`,
  apiBase_url = `http://${running_url}/${apiPath}/`;

//uncomment these four line for use staging

// const running_url = staging,
//   frontEndUrl = `https://${frontEndStaging}`,
//   http_url = `https://${running_url}`,
//   apiBase_url = `https://${running_url}/${apiPath}/`;

export default class Connection {
  static getResturl() {
    return apiBase_url;
  }
  static getCmsUrl() {
    return frontEndUrl;
  }
  static getBaseUrl() {
    return http_url;
  }
  static getSuccessUrl() {
    return `${apiBase_url}success.html`;
  }
  static getErroUrl() {
    return `${apiBase_url}failure.html`;
  }
}
