/*
Name : Suraj Sanwal
File Name : Styles.js
Description : Contains Style for Auth button component.
Date : 12 Sept 2018
*/

import { StyleSheet } from "react-native";
import Constants from "../../../constants";
import { moderateScale } from "../../../helpers/ResponsiveFonts";

export default StyleSheet.create({
  buttonContainer: {
    // alignSelf: "center"
  },
  gradientStyle: {
    borderRadius: moderateScale(50),
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    padding: moderateScale(20)
  },
  buttonText: {
    ...Constants.Fonts.TitilliumWebSemiBold,
    fontSize: moderateScale(16),
    fontWeight: "bold",
    color: Constants.Colors.Yellow,
    textAlignVertical: "center",
    paddingHorizontal: moderateScale(5)
  }
});
