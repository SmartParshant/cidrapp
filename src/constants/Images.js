/*
 * @file: Images.js
 * @description: Images constants file for the application

 * */

"use strict";
module.exports = {
  Common: {
    Logo: require("../assets/images/logo.png"),
    Arrow: require("../assets/images/arrow-1.png"),
    Admin: require("../assets/images/admin.png"),
    Driver: require("../assets/images/driver.png"),
    Rider: require("../assets/images/rider.png"),
    Background: require("../assets/images/bg.png"),
    DashBG: require("../assets/images/dash_bg.png"),
    Back: require("../assets/images/back_arrow.png"),
    Next: require("../assets/images/arrow_btn.png"),
    Successful_signed: require("../assets/images/Successfully_signed.png"),
    User: require("../assets/images/username.png"),
    Password: require("../assets/images/password.png"),
    Email: require("../assets/images/email.png"),
    Phone: require("../assets/images/phone.png"),
    Forgot_password: require("../assets/images/Forgot_password.png"),
    Popdown: require("../assets/images/arrow.png"),
    Cancel: require("../assets/images/Cancelled.png"),
    Accept: require("../assets/images/Accepted.png"),
    YellowAccept: require("../assets/images/Active_check.png"),
    Cross: require("../assets/images/Cross.png"),
    Source: require("../assets/images/source.png"),
    Destination: require("../assets/images/destination.png"),
    Bus: require("../assets/images/bus.png"),
    NextArrow: require("../assets/images/arrow.png"),
    Search: require("../assets/images/Search.png"),
    Provider: require("../assets/images/Provider.png"),
    Edit: require("../assets/images/edit.png"),
    UpArrow: require("../assets/images/up_arrow.png"),
    Thankyou: require("../assets/images/Thank_you.png"),
    Chat: require("../assets/images/message.png"),
    WhiteChat: require("../assets/images/chat3.png"),
    Filter: require("../assets/images/Filter.png"),
    Sort: require("../assets/images/Sort.png")
  },
  TabIcon: {
    Tab1: require("../assets/images/list.png"),
    Tab2: require("../assets/images/swap.png")
  },
  Drawer: {
    Toggle: require("../assets/images/Drawer.png"),
    Home: require("../assets/images/Home.png"),
    History: require("../assets/images/Trip.png"),
    Rewards: require("../assets/images/Reward.png"),
    Feedback: require("../assets/images/feedback.png"),
    Myprofile: require("../assets/images/My_Profile.png"),
    Faq: require("../assets/images/FAQs.png"),
    Settings: require("../assets/images/setting.png"),
    Support: require("../assets/images/support.png"),
    Termspolicy: require("../assets/images/policy.png"),
    Logout: require("../assets/images/Logout.png")
  },
  Signup: {},
  Login: {
    Logo: require("../assets/images/logo-dark-bg.png")
  },
  Profile: {
    PhotoCamera: require("../assets/images/photo-camera.png")
  },
  Dashboard: {
    PicupLocation: require("../assets/images/pin_location.png")
  },
  RideInfo: {
    Arrow: require("../assets/images/arrow.png"),
    Dropdown: require("../assets/images/Dropdown.png"),
    Clock: require("../assets/images/clock.png"),
    Man: require("../assets/images/man.png"),
    InActiveShuttle: require("../assets/images/inactive_Shuttle.png"),
    ActiveShuttle: require("../assets/images/active_Shuttle.png"),
    ClockGray: require("../assets/images/clock_1.png")
  },
  TerminalDetail: {
    Pin: require("../assets/images/pin.png")
  },
  Rating: {
    StarInActive: require("../assets/images/star_inactive.png"),
    StarActive: require("../assets/images/Star.png"),
    RateOurApp: require("../assets/images/Rate_our_App.png")
  }
};
