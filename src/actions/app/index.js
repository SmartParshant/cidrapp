import * as types from "../../actionTypes";
import { toastMessage } from "../../config/navigators";
import { Navigator } from "react-native-navigation";
import Constants from "../../constants";
let navigator = new Navigator();
let { push, pop, resetTo } = navigator;

export function appInitialized() {
  //return async function(dispatch, getState) {
  return async function(dispatch) {
    // since all business logic should be inside redux actions
    // this is a good place to put your app initialization code
    dispatch(changeAppRoot("LoginScreen"));
  };
}
export function serverError(navigator) {
  toastMessage(navigator, {
    type: Constants.AppCosntants.Notificaitons.Error,
    message: Constants.AppCosntants.Error.serverError
  });
}
export function changeAppRoot(root) {
  return { type: types.ROOT_CHANGED, root: root };
}

export function login() {
  return async function(dispatch) {
    // login logic would go here, and when it's done, we switch app roots
    dispatch(changeAppRoot("login"));
  };
}

export function movedashBoardTab() {
  return async function(dispatch) {
    // login logic would go here, and when it's done, we switch app roots
    dispatch(changeAppRoot("after-login"));
  };
}

/*
Move to specified screen
*/
export const moveToScreen = root => ({
  type: types.ROOT_CHANGED,
  root
});

/*
Setting the navigator
*/
export const setNavigator = navigator => ({
  type: types.SET_NAVIGATOR,
  payload: navigator
});

export const pushToScreen = (screen, passProps = {}) => async (dispatch, getState) => {
  await push({
    screen: screen,
    passProps: { ...passProps, ...navigator }
  });
};

export const resetToScreen = (screen, passProps = {}) => async (dispatch, getState) => {
  await resetTo({
    screen: screen,
    passProps: { ...passProps, ...navigator }
  });
};
