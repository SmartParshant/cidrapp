import { Platform } from "react-native";
import RestClient from "../../helpers/RestClient";
import { serverError } from "../app";
import * as Types from "../../actionTypes/index";
import { toastMessage } from "../../config/navigators";
import Constants from "../../constants";
// import { requestTrip, riderCancelTripRequest } from "../../helpers/socket";
import UserSocket from "../../helpers/socket/rider";
import MapApi from "../../helpers/Maps";

export const setLocationType = (location, navigator, push = true, admin = "", sourceAdmin = {}, onSelectTerminal) => {
  return dispatch => {
    dispatch({
      type: Types.PICKUP_POINT_REQUEST
    });
    dispatch({
      type: Types.RIDER_LOCATION_TYPE,
      payload: location
    });
    if (push) {
      navigator.push({
        screen: "RiderTerminal",
        passProps: { admin, onSelectTerminal, sourceAdmin },
        animated: true,
        animationType: "slide-horizontal"
      });
    }
  };
};
export const cancleRide = navigator => {
  return () => {
    UserSocket.riderCancelTripRequest();
    navigator.dismissModal();
  };
};

export const setRiderLocation = (location, navigator) => {
  return (dispatch, getState) => {
    let { locationType, destination, source } = getState().riderLocation;
    let sourceCord = {
      latitude: source.loc && source.loc[1],
      longitude: source.loc && source.loc[0]
    };
    let destinationCord = {
      latitude: destination.loc && destination.loc[1],
      longitude: destination.loc && destination.loc[0]
    };
    if (locationType == Constants.AppCosntants.UserLocation.Source) {
      dispatch({
        type: Types.RIDER_SOURCE,
        payload: location
      });
    } else {
      dispatch({
        type: Types.RIDER_DESTINATION,
        payload: location
      });
      if (location.loc) {
        destinationCord = {
          latitude: location.loc && location.loc[1],
          longitude: location.loc && location.loc[0]
        };
      }
      if (destination.loc && source.loc) {
        MapApi.getRegionForCoordinates([sourceCord, destinationCord]).then(region =>
          dispatch({
            type: Types.UPDATE_REGION,
            payload: region
          })
        );
      }
    }
    if (location && location.loc) {
      navigator.pop();
    }
  };
};

export const updateRiders = (riders, navigator) => {
  return dispatch => {
    navigator.dismissModal({
      animationType: "slide-down"
    });
    dispatch({
      type: Types.RIDER_COUNT,
      payload: riders
    });
  };
};

export const updateRideTime = time => {
  return dispatch => {
    dispatch({
      type: Types.UPDATE_RIDE_TIME,
      payload: time
    });
  };
};

export const updateRideDetails = () => {
  return () => {
    UserSocket.requestTrip();
  };
};
export const getServiceProviders = (qry = "", navigator) => {
  return dispatch => {
    dispatch({
      type: Types.PROVIDER_REQUEST
    });
    RestClient.getCall(`users/admins?name=${qry}`)
      .then(res => {
        if (res.success) {
          dispatch({
            type: Types.PROVIDERS,
            payload: res.data
          });
          dispatch({
            type: Types.PROVIDER_SUCESS
          });
        } else {
          dispatch({
            type: Types.PROVIDER_REQUEST_FAILS
          });
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({
          type: Types.PROVIDER_REQUEST_FAILS
        });
        serverError(navigator);
      });
  };
};

export const setProvider = (provider, navigator, isSearch = false) => {
  return dispatch => {
    if (isSearch) {
      if (Platform.OS === "ios") {
        setTimeout(() => {
          navigator.dismissModal();
        }, 600);
        navigator.handleDeepLink({
          link: "DashBoard",
          payload: {
            push: true
          }
        });
      } else {
        setTimeout(() => {
          navigator.push({
            screen: "DashBoard",
            animated: true,
            animationType: "slide-horizontal"
          });
        }, 600);
        navigator.dismissModal();
      }
    } else {
      navigator.handleDeepLink({
        link: "DashBoard",
        payload: {
          push: true
        }
      });
    }
    dispatch({
      type: Types.USER_PROVIDER,
      payload: provider
    });
  };
};
export const getRiderPickupPoints = (qry = "", navigator, admin) => {
  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    let { userProvider } = getState().riderLocation;
    dispatch({
      type: Types.PICKUP_POINT_REQUEST
    });
    RestClient.getCall(`users/location/fromterminals?adminId=${admin || userProvider._id}&name=${qry}`, accessToken)
      .then(res => {
        if (res.success) {
          dispatch({
            type: Types.PICKUP_POINT,
            payload: res.data.locations
          });
          dispatch({
            type: Types.PICKUP_POINT_SUCESS
          });
        } else {
          dispatch({
            type: Types.PICKUP_POINT_REQUEST_FAILS
          });
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({
          type: Types.PICKUP_POINT_REQUEST_FAILS
        });
        serverError(navigator);
      });
  };
};

export const getRiderDropupPoints = (qry = "", navigator, admin, sourceAdmin) => {
  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    let { userProvider, source } = getState().riderLocation;
    dispatch({
      type: Types.PICKUP_POINT_REQUEST
    });
    RestClient.getCall(
      `users/location/toterminals?source=[${admin ? sourceAdmin.loc[0] : source.loc[0]},${
        admin ? sourceAdmin.loc[1] : source.loc[1]
      }]&adminId=${admin || userProvider._id}&name=${qry}`,
      accessToken
    )
      .then(res => {
        if (res.success) {
          dispatch({
            type: Types.PICKUP_POINT,
            payload: res.data.locations
          });
          dispatch({
            type: Types.PICKUP_POINT_SUCESS
          });
        } else {
          dispatch({
            type: Types.PICKUP_POINT_REQUEST_FAILS
          });
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({
          type: Types.PICKUP_POINT_REQUEST_FAILS
        });
        serverError(navigator);
      });
  };
};

//check if rider has any active ride or not if yes return ride data
export const getRideData = navigator => {
  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    RestClient.getCall("users/ridernotificationrequests", accessToken)
      .then(res => {
        if (res.success) {
          dispatch({
            type: Types.UPDATE_TRIP,
            payload: res.data
          });
        }
      })
      .catch(() => {
        serverError(navigator);
      });
  };
};
export const resetTerminals = () => {
  return dispatch => {
    dispatch({
      type: Types.RESET_TERMINAL_LIST
    });
  };
};

export const goToHome = navigator => {
  return dispatch => {
    setTimeout(() => {
      navigator.handleDeepLink({ link: "RiderProviderListing" });
    }, 500);
    dispatch({ type: Types.RESET_RIDER_DATA });
    dispatch({ type: Types.RESET_RIDER_TRIP });
    navigator.dismissAllModals();
  };
};

export const updateRegion = region => {
  return dispatch => {
    dispatch({
      type: Types.UPDATE_REGION,
      payload: region
    });
  };
};

export const rateAndReview = (postData, navigator) => {
  // let example={
  //   reviewerId: "5baa2a790753f76deeadde5a",
  //    reviewToId: "5baa1208938c135acbb0130b",
  //     reviewToType: "admin",
  //     message: "Testing By RJ admin",
  //     rating: "3"};

  // in case of super admin reviewToId will be null

  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    dispatch({
      type: Types.RIDER_RATE_REVIEW_REQUEST
    });
    RestClient.restCall("users/addReview", postData, accessToken, "post")
      .then(res => {
        if (res.success) {
          dispatch({
            type: Types.RIDER_RATE_REVIEW,
            payload: res
          });
          toastMessage(navigator, {
            type: Constants.AppCosntants.Notificaitons.Success,
            message: res.message
          });
          dispatch({
            type: Types.RIDER_RATE_REVIEW_SUCESS
          });
          if (postData.reviewToType === Constants.AppCosntants.UserTypes.SuperAdmin) {
            navigator.handleDeepLink({
              link: "DashBoard",
              payload: {
                push: true
              }
            });
          } else if (postData.reviewToType === Constants.AppCosntants.UserTypes.Admin) {
            dispatch({ type: Types.SET_RATE_SCREEN, payload: Constants.AppCosntants.RideStatus.RatingDriver });
            navigator.push({
              screen: "RiderRateToDriver",
              animated: true,
              animationType: "slide-horizontal"
            });
          } else {
            setTimeout(() => {
              navigator.resetTo({
                screen: "RiderProviderListing",
                animated: true,
                animationType: "slide-horizontal"
              });
            }, 2000);
            dispatch({ type: Types.RESET_RIDER_DATA });
            dispatch({ type: Types.RESET_RIDER_TRIP });
            navigator.push({
              screen: "Thankyou",
              animated: true,
              animationType: "fade",
              passProps: {}
            });
          }
        } else {
          toastMessage(navigator, {
            type: Constants.AppCosntants.Notificaitons.Error,
            message: res.message
          });
          dispatch({
            type: Types.RIDER_RATE_REVIEW_FAIL
          });
        }
      })
      .catch(() => {
        dispatch({
          type: Types.RIDER_RATE_REVIEW_FAIL
        });
        serverError(navigator);
      });
  };
};

export const thankyou = navigator => {
  return dispatch => {
    setTimeout(() => {
      navigator.resetTo({
        screen: "RiderProviderListing"
      });
    }, 2000);
    dispatch({ type: Types.RESET_RIDER_DATA });
    dispatch({ type: Types.RESET_RIDER_TRIP });
    navigator.push({
      screen: "Thankyou",
      animated: true,
      animationType: "fade",
      passProps: {}
    });
  };
};

export const riderRating = navigator => {
  return dispatch => {
    dispatch({ type: Types.SET_RATE_SCREEN, payload: Constants.AppCosntants.RideStatus.RatingRide });
    navigator.push({
      screen: "RiderRating"
    });
  };
};

export const riderDriverRating = navigator => {
  return dispatch => {
    dispatch({ type: Types.SET_RATE_SCREEN, payload: Constants.AppCosntants.RideStatus.RatingDriver });
    navigator.push({
      screen: "RiderRateToDriver"
    });
  };
};
