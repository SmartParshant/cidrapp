import RestClient from "../../helpers/RestClient";
import { toastMessage } from "../../config/navigators";
import { changeAppRoot, serverError } from "../app";
import * as Types from "../../actionTypes/index";
import Constants from "../../constants";
// import { disconnectSocket } from "../../helpers/socket";
import UserSocket from "../../helpers/socket/rider";
import DriverSocket from "../../helpers/socket/driver";
import AdminSocket from "../../helpers/socket/admin";
/*
Api for registeration of rider
*/

export const registeration = (postData, navigator) => {
  return dispatch => {
    dispatch({ type: Types.SIGNUP_REQUEST });
    RestClient.restCall("users/register", postData)
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.SIGNUP_SUCESS });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Success, message: res.message });
          dispatch({ type: Types.SAVE_ACCESS_TOKEN, payload: res.data.jwtAccessToken });
          dispatch({ type: Types.SAVE_USER, payload: res.data.user });
          navigator.push({
            screen: "OTPScreen",
            animated: true,
            animationType: "slide-horizontal",
            passProps: { user: res.data.user }
          });
        } else {
          dispatch({ type: Types.SIGNUP_FAIL });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.SIGNUP_FAIL });
        serverError(navigator);
      });
  };
};

export const signIn = (postData, navigator) => {
  return (dispatch, getState) => {
    dispatch({ type: Types.LOGIN_REQUEST });
    RestClient.restCall("auth/login", postData)
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.LOGIN_SUCESS });
          dispatch({ type: Types.SAVE_ACCESS_TOKEN, payload: res.data.jwtAccessToken });
          dispatch({ type: Types.SAVE_USER, payload: res.data.user });
          if (getState().user.mobileVerified) {
            dispatch(changeAppRoot("after-login"));
          } else {
            navigator.push({
              screen: "OTPScreen",
              animated: true,
              animationType: "slide-horizontal",
              passProps: {}
            });
          }
        } else {
          dispatch({ type: Types.LOGIN_FAIL });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.LOGIN_FAIL });
        serverError(navigator);
      });
  };
};

export const forgotPassword = (postData, navigator) => {
  return dispatch => {
    dispatch({ type: Types.FORGOT_REQUEST });
    RestClient.restCall("config/forgot", postData)
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.FORGOT_SUCESS });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Success, message: res.message });
          navigator.resetTo({
            screen: "LoginScreen",
            animated: true,
            animationType: "slide-horizontal",
            passProps: {}
          });
        } else {
          dispatch({ type: Types.FORGOT_FAIL });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.FORGOT_FAIL });
        serverError(navigator);
      });
  };
};

export const verifyOTP = (postData, navigator) => {
  return (dispatch, getState) => {
    dispatch({ type: Types.OTP_REQUEST });
    RestClient.restCall("verify/mobile", postData, getState().user.accessToken)
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.OTP_SUCESS });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Success, message: res.message });
          dispatch({ type: Types.SAVE_USER, payload: res.data });
          setTimeout(() => {
            if (getState().app.root == "after-login") {
              navigator.popToRoot({
                animated: true,
                animationType: "fade"
              });
            } else {
              dispatch(changeAppRoot("after-login"));
            }
          }, 2000);
          navigator.push({
            screen: "OTPSucess",
            animated: true,
            animationType: "slide-horizontal",
            passProps: {}
          });
        } else {
          dispatch({ type: Types.OTP_FAIL });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.OTP_FAIL });
        serverError(navigator);
      });
  };
};

export const resendOTP = (postData, navigator) => {
  return (dispatch, getState) => {
    dispatch({ type: Types.OTP_REQUEST });
    RestClient.restCall("users/resendOtp", postData, getState().user.accessToken)
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.OTP_SUCESS });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Success, message: res.message });
        } else {
          dispatch({ type: Types.OTP_FAIL });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.OTP_FAIL });
        serverError(navigator);
      });
  };
};
export const logout = navigator => {
  return (dispatch, getState) => {
    let { accessToken, deviceToken, deviceType, userType } = getState().user;
    let postData = { device: { token: deviceToken, type: deviceType } };
    RestClient.restCall("auth/logout", postData, accessToken, "PUT")
      .then(res => {
        if (res.success) {
          if (userType === Constants.AppCosntants.UserTypes.Admin) {
            AdminSocket.disconnectSocketAdmin();
          } else if (userType === Constants.AppCosntants.UserTypes.Driver) {
            DriverSocket.disconnectSocket();
          } else {
            UserSocket.disconnectSocket();
          }
          dispatch({ type: Types.RESET_RIDER_DATA });
          dispatch({ type: Types.RESET_TRIP });
          dispatch({ type: Types.RESET_SHUTTLE });
          dispatch({ type: Types.RESET_TERMINAL_LISTING });
          dispatch({ type: Types.RESET_USER });
          dispatch({ type: Types.RESET_RIDER_TRIP });
          dispatch({ type: Types.RESET_APP });
        } else {
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        serverError(navigator);
      });

    // it will navigate to login screen and then other data will be clear
  };
};
export const updateNotificationsInfo = payload => {
  return dispatch => {
    dispatch({ type: Types.UPDATE_NOTIFICATIONS_INFO, payload: payload });
  };
};

//Get CMS Content
export const getCMSContent = (uri, navigator, callback) => {
  return dispatch => {
    dispatch({ type: Types.SHOW_CMS_LOADER });
    RestClient.getCall(uri)
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.HIDE_CMS_LOADER });
          callback(res);
        } else {
          dispatch({ type: Types.HIDE_CMS_LOADER });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.HIDE_CMS_LOADER });
        serverError(navigator);
      });
  };
};
