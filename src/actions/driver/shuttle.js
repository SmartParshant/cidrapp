import RestClient from "../../helpers/RestClient";
import { toastMessage } from "../../config/navigators";
import { serverError } from "../app";
import * as Types from "../../actionTypes/index";
import Constants from "../../constants";

export const getDriverShuttle = navigator => {
  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    dispatch({ type: Types.SHUTTLE_LIST_REQUEST });
    RestClient.getCall("users/drivers/shuttles", accessToken)
      .then(res => {
        if (res.success) {
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Success, message: res.message });
          dispatch({ type: Types.SHUTTLE_LIST, payload: res.data });
          dispatch({ type: Types.SHUTTLE_LIST_SUCESS });
        } else {
          dispatch({ type: Types.SHUTTLE_LIST_FAIL });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.SHUTTLE_LIST_FAIL });
        serverError(navigator);
      });
  };
};

export const updateTripStatus = (data, navigator) => {
  // data={
  //   shuttle:{},
  //   tripId:"",
  //   status:true/false, shuttle status for updation
  //   driverId:""
  // }
  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    dispatch({ type: Types.TRIP_UPDATE_REQUEST });
    //dispatch({ type: Types.RESET_TRIP });
    RestClient.restCall(
      `users/drivers/updateShuttleStatus?shuttleId=${data.shuttle._id}&driverId=${data.driverId}&activeStatus=${
        data.status
      }&id=${data.tripId}`,
      {},
      accessToken,
      "PUT"
    )
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.TRIP_UPDATE_REQUEST_SUCCESS });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Success, message: res.message });
          if (data.status) {
            setTimeout(() => {
              navigator.handleDeepLink({
                link: "Maps",
                payload: {
                  push: true
                }
              });
            }, 500);
            dispatch({ type: Types.SELECTED_SHUTTLE, payload: data.shuttle });
            dispatch({ type: Types.UPDATE_TRIP_DATA, payload: res.data });
          } else {
            setTimeout(() => {
              navigator.handleDeepLink({
                link: "SelectShuttle"
              });
            }, 100);
            navigator.dismissModal();
            dispatch({ type: Types.RESET_TRIP });
          }
        } else {
          dispatch({ type: Types.TRIP_UPDATE_REQUEST_FAIL });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.TRIP_UPDATE_REQUEST_FAIL });
        serverError(navigator);
      });
  };
};

/**
 *
 * @param {*} navigator
 */
export const getRideRequests = navigator => {
  return (dispatch, getState) => {
    let { accessToken, userType } = getState().user;
    let { response } = getState().trip;
    let { currentTrip } = getState().listing;
    let terminalId = "";
    dispatch({ type: Types.RIDE_REQUEST_LIST_REQUEST });
    RestClient.getCall(
      `users/drivers/terminalRideRequests?tripId=${
        userType === Constants.AppCosntants.UserTypes.Driver ? response._id : currentTrip
      }&terminalId=${terminalId}`,
      accessToken
    )
      .then(res => {
        if (res.success) {
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Success, message: res.message });
          dispatch({ type: Types.RIDE_REQUEST_LIST, payload: res.data });
          dispatch({ type: Types.RIDE_REQUEST_LIST_SUCESS });
        } else {
          dispatch({ type: Types.RIDE_REQUEST_LIST_FAIL });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.RIDE_REQUEST_LIST_FAIL });
        serverError(navigator);
      });
  };
};

export const getTripHistory = (page, navigator) => {
  return (dispatch, getState) => {
    let { accessToken, _id } = getState().user;
    if (page == 1) {
      dispatch({ type: "RESET_HISTORY" });
    }
    dispatch({ type: Types.TRIP_HISTORY_REQUEST });
    RestClient.getCall(`/users/drivers/driverHistory?id=${_id}&pageNo=${page}`, accessToken)
      .then(res => {
        if (res.success) {
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Success, message: res.message });
          dispatch({ type: Types.TRIP_HISTORY, payload: res.data });
          dispatch({ type: Types.TRIP_HISTORY_REQUEST_SUCESS });
        } else {
          dispatch({ type: Types.TRIP_HISTORY_REQUEST_FAIL });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.TRIP_HISTORY_REQUEST_FAIL });
        serverError(navigator);
      });
  };
};

export const getTerminalListing = (terminalId, navigator) => {
  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    let { response } = getState().trip;
    dispatch({ type: Types.TERMINAL_RIDE_REQUEST_LIST_REQUEST });
    RestClient.getCall(
      `users/drivers/terminalRideRequests?tripId=${response && response._id}&terminalId=${terminalId}`,
      accessToken
    )
      .then(res => {
        if (res.success) {
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Success, message: res.message });
          dispatch({ type: Types.TERMINAL_RIDE_REQUEST_LIST, payload: res.data });
          dispatch({ type: Types.TERMINAL_RIDE_REQUEST_LIST_SUCESS });
        } else {
          dispatch({ type: Types.TERMINAL_RIDE_REQUEST_LIST_FAIL });
          toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.TERMINAL_RIDE_REQUEST_LIST_FAIL });
        serverError(navigator);
      });
  };
};
