import RestClient from "../../helpers/RestClient";
import { Platform } from "react-native";
// import { toastMessage } from "../../config/navigators";
import { serverError } from "../app";
import * as Types from "../../actionTypes/index";
import MapApi from "../../helpers/Maps";
// import Constants from "../../constants";

export const getShuttleListing = (pageNo, locationId, navigator) => {
  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    if (pageNo === 1) {
      dispatch({ type: Types.ADMIN_SHUTTLE_LISTING_RESET });
    }
    dispatch({ type: Types.ADMIN_SHUTTLE_LISTING_REQUEST });
    RestClient.getCall(`admin/mobile/vehicles?locationId=${locationId}&pageNo=${pageNo}&limit=20`, accessToken)
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.ADMIN_SHUTTLE_META, payload: res.data.meta });
          dispatch({ type: Types.ADMIN_SHUTTLE_LIST, payload: res.data.shuttles });
          dispatch({ type: Types.ADMIN_SHUTTLE_LISTING_SUCESS });
        } else {
          dispatch({ type: Types.ADMIN_SHUTTLE_LISTING_FAIL });
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.ADMIN_SHUTTLE_LISTING_FAIL });
        serverError(navigator);
      });
  };
};

export const getDriverListing = (pageNo, locationId, navigator) => {
  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    dispatch({ type: Types.ADMIN_DRIVER_LISTING_REQUEST });
    RestClient.getCall(`admin/mobile/drivers?locationId=${locationId}&pageNo=${pageNo}&limit=20`, accessToken)
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.ADMIN_DRIVER_META, payload: res.data.meta });
          dispatch({ type: Types.ADMIN_DRIVER_LIST, payload: res.data.drivers });
          dispatch({ type: Types.ADMIN_DRIVER_LISTING_SUCESS });
        } else {
          dispatch({ type: Types.ADMIN_DRIVER_LISTING_FAIL });
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.ADMIN_DRIVER_LISTING_FAIL });
        serverError(navigator);
      });
  };
};

export const getRiderListing = (pageNo, locationId, navigator) => {
  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    if (pageNo === 1) {
      dispatch({ type: Types.ADMIN_RIDER_LISTING_RESET });
    }
    dispatch({ type: Types.ADMIN_RIDER_LISTING_REQUEST });
    RestClient.getCall(`admin/mobile/rides?locationId=${locationId}&pageNo=${pageNo}&limit=20`, accessToken)
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.ADMIN_RIDER_LIST, payload: res.data.trips });
          dispatch({ type: Types.ADMIN_RIDER_META, payload: res.data.meta });
          dispatch({ type: Types.ADMIN_RIDER_LISTING_SUCESS });
        } else {
          dispatch({ type: Types.ADMIN_RIDER_LISTING_FAIL });
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.ADMIN_RIDER_LISTING_FAIL });
        serverError(navigator);
      });
  };
};

export const updateFilters = (filters, navigator) => {
  return dispatch => {
    dispatch({ type: Types.UPDATE_FILTERS, payload: filters });
    navigator.pop();
  };
};

/**
 *
 * @param {*} pageNo page no for rides
 * @param {*} locationId if required any location based active shuttles
 * @param {*} navigator navigator for perform navigation action
 */
export const ActiveTrips = (pageNo, locationId, navigator) => {
  return (dispatch, getState) => {
    let { accessToken } = getState().user;
    dispatch({ type: Types.ADMIN_ACTIVE_TRIPS_REQUEST });
    RestClient.getCall(`admin/mobile/activetrips?locationId=${locationId}&pageNo=${pageNo}&limit=20`, accessToken)
      .then(res => {
        if (res.success) {
          let reg = [];
          dispatch({ type: Types.ADMIN_ACTIVE_TRIPS, payload: res.data });
          dispatch({ type: Types.ADMIN_ACTIVE_TRIPS_SUCCESS });
          res.data.length > 0 &&
            res.data.map(item => {
              if (item.gpsLoc) {
                reg.push({ longitude: item.gpsLoc[0], latitude: item.gpsLoc[1] });
              }
            });
          if (reg.length > 0) {
            MapApi.getCenterCordinates(reg).then(region => {
              dispatch({ type: Types.UPDATE_REGION, payload: region });
            });
          }
        } else {
          dispatch({ type: Types.ADMIN_ACTIVE_TRIPS_FAIL });
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.ADMIN_ACTIVE_TRIPS_FAIL });
        serverError(navigator);
      });
  };
};
/**
 *
 * @param {*} tripId
 * @param {*} navigator
 */
export const updateCurrentTrip = (tripId, navigator) => {
  return dispatch => {
    setTimeout(() => {
      if (Platform.OS == "ios") {
        navigator.handleDeepLink({
          link: "TripMap",
          payload: {
            push: true
          }
        });
      } else {
        navigator.push({
          screen: "TripMap"
        });
      }
    }, 500);
    dispatch({ type: Types.ADMIN_UPDATE_CURRENT_TRIP, payload: tripId });
    navigator.dismissModal();
  };
};

/**
 *
 */
export const getTripRoute = navigator => {
  return (dispatch, getState) => {
    let { user, listing } = getState();
    let { accessToken } = user;
    let { currentTrip } = listing;
    dispatch({ type: Types.ADMIN_CURRENT_TRIP_ROUTE_REQUEST });
    RestClient.getCall(`/admin/trip/details/route?tripID=${currentTrip}`, accessToken)
      .then(res => {
        if (res.success) {
          dispatch({ type: Types.ADMIN_CURRENT_TRIP_ROUTE, payload: res.data.driverRoute });
          dispatch({ type: Types.ADMIN_ACTIVE_TRIPS_SUCCESS });
        } else {
          dispatch({ type: Types.ADMIN_CURRENT_TRIP_ROUTE_FAIL });
          // toastMessage(navigator, { type: Constants.AppCosntants.Notificaitons.Error, message: res.message });
        }
      })
      .catch(() => {
        dispatch({ type: Types.ADMIN_CURRENT_TRIP_ROUTE_FAIL });
        serverError(navigator);
      });
  };
};
